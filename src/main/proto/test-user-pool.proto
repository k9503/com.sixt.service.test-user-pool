syntax = "proto3";

package com.sixt.service.test_user_pool.api;

option java_multiple_files = true;
option java_package = "com.sixt.service.test_user_pool.api";

enum UserType {
	CLASSIC = 0; // An un-imported user on classic stack.
	VANILLA = 1; // Fresh registered user through users-api, email confirmed.
	SAC = 2; // Ready to use product Share-A-Car.
	//DSS = 3; // OBSOLETE
	FASTLANE = 4; // Ready to use product Fastlane.
	SAC_IT = 5; // Italian specific user for using product Share-A-Car.
	SAC_NL = 6; // With default language set to Dutch.
	SAC_DE = 7; // With default language set to German.
	SAC_EN = 8; // With default language set to English.
	SAC_AT = 9; // With default language set to English.

	// OBSOLETE
	reserved 3;
	reserved "DSS";
}

/* GetUserRequest represents a request to get a test user from the pool,
 * with specifying a type of product and an optional testcase/class identifier.
 */
message GetUserRequest {
	UserType user_type = 1;
	string tc_id = 2; // The identifier for the testcase / testclass which used this user.
}

/* GetUserResponse contains the user login as a name:password pair.
 */
message GetUserResponse {
	string name = 1;
	string pwd = 2;
	string pin = 3;
	string cc = 4;
	enum Error {
		INTERNAL_ERROR = 0;
		BAD_REQUEST = 1;
		UNDEFINED_USERTYPE = 2;
		UNDEFINED_TCID = 3;
		OUT_OF_TESTUSER = 4;
	}
}

/* LookupUserRequest contains only the user name look after.
 */
message LookupUserRequest {
	string name = 1;
}

/* LookupUserResponse contains the password of the test user and the test which "owns" it.
 */
message LookupUserResponse {
	string pwd = 1;
	UserType user_type = 2;
	string tc_id = 3;
	enum Error {
		INTERNAL_ERROR = 0;
		NOT_FOUND = 1;
	}
}

/* TestUserPool provides a ready-to-use, one-time-disposable user for a specific type(product).
*/
service TestUserPool {
	// GetUser gets a test user's login data in name:password pair.
	// This is the main endpoint a client should request.
	rpc GetUser(GetUserRequest) returns (GetUserResponse);

	//LookupUser retrieves the password of a already used test user.
	rpc LookupUser(LookupUserRequest) returns (LookupUserResponse);
}
