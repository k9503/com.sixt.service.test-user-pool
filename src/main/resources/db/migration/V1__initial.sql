create table test_user (
  name text PRIMARY KEY,
  pwd text,
  user_type text,
  tc_id text,
  creation_time timestamp without time zone,
  blocked_time timestamp without time zone,
  blocked bool
);

CREATE INDEX IF NOT EXISTS name_index ON test_user(name);

ALTER TABLE test_user ADD CONSTRAINT name_uniq UNIQUE(name);
