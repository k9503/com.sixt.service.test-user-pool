package com.sixt.service.test_user_pool;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import okhttp3.OkHttpClient;

@Configuration
public class ApplicationConfig {

	@Bean
	public OkHttpClient okHttpClient() {
		return new OkHttpClient();
	}
}
