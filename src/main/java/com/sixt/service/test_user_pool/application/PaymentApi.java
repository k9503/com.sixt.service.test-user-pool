package com.sixt.service.test_user_pool.application;

import com.google.gson.Gson;

import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;
import com.sixt.service.test_user_pool.domain.UserData;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import okhttp3.Headers;
import okhttp3.Headers.Builder;
import okhttp3.MediaType;


@Component
public class PaymentApi {
	private static final Logger LOG = LoggerFactory.getLogger(PaymentApi.class);
	private final MediaType JSON = MediaType.get("application/json; charset=utf-8");
	private Gson gson = new Gson();
	private HttpClientBase client = HttpClientBase.getInstance();

	// k8s deployment.yaml --> SPRING_PROFILES_ACTIVE: <dev | stage>
	// application-<SPRING_PROFILES>.yaml --> app.host
	@Value("${app.host}")
	private String host;

	@Value("${payment.host}")
	private String paymentHost;

	public PaymentApi(
			@Value("${app.host}") String host,
			@Value("${payment.host}") String paymentHost) {
			this.host = host;
			this.paymentHost = paymentHost;
			}

	/**
	 * @return the paymentHost
	 */
	public String getPaymentHost() {
		return paymentHost;
	}

	/**
	 * @param paymentHost the paymentHost to set
	 */
	public void setPaymentHost(String paymentHost) {
		this.paymentHost = paymentHost;
	}

	/**
	 * @param host the host to set
	 */
	public void setHost(String host) {
		this.host = host;
	}

	/**
	 * @return the host
	 */
	public String getHost() {
		return host;
	}

	/**
	 * Endpoint: /v1/payment/instruments/signature
	 * Method: POST
	 */
	public String signature(String uid, String token) {
		final String url = host + "/v1/payment/instruments/signature";
		final String walletOwnerSource = "sixt-one";

		JsonObject payload = new JsonObject();
		payload.addProperty("walletOwnerSource", walletOwnerSource);
		payload.addProperty("walletOwnerId", uid);

		try {
			Headers headers = new Builder()
				.set("Authorization", "Bearer " + token)
				.build();
			SignatureSuccessResponse response = gson.fromJson(client.post(url, payload.toString(), headers, JSON), SignatureSuccessResponse.class);
			return response.getSignature();
		} catch (Exception e) {
			LOG.error("Error signature the payment method. {}", e.getMessage());
			return null;
		}
	}

	private class SignatureSuccessResponse {
		private String signature;

		private String getSignature() {
			return signature;
		}
	}

	/**
	 * Endpoint: /v2/register?signature=
	 * Method: POST
	 */
	public String register(String signature, String cardHolderName) {
		final String url = paymentHost + "/v2/register?signature=" + signature;

		JsonObject paymentMethod = new JsonObject();
		paymentMethod.addProperty("brand", UserData.CC.BRAND);

		JsonObject cardOwner = new JsonObject();
		cardOwner.addProperty("name", cardHolderName);
		paymentMethod.add("card_owner", cardOwner);

		JsonObject expiration = new JsonObject();
		expiration.addProperty("month", UserData.CC.EXPIRATION_MONTH);
		expiration.addProperty("year", UserData.CC.EXPIRATION_YEAR);
		paymentMethod.add("expiration", expiration);

		paymentMethod.addProperty("pan", UserData.CC.PAN); // Replacing 5340375541689963
		paymentMethod.addProperty("security_code", UserData.CC.SEC_CODE);

		JsonObject threeDS = new JsonObject();
		threeDS.addProperty("channel", "android");

		JsonObject jsonBody = new JsonObject();
		jsonBody.add("payment_method", paymentMethod);
		jsonBody.add("3ds", threeDS);

		try {
			Headers headers = new Builder().build();
			RegisterSuccessResponse response = gson.fromJson(client.post(url, jsonBody.toString(), headers, JSON), RegisterSuccessResponse.class);
			return response.getThreeDs().getResultCode();
		} catch (Exception e) {
			LOG.error("Error register payment method. {}", e.getMessage());
			return null;
		}
	}

	private class RegisterSuccessResponse {
		@SerializedName("3ds")
		private ThreeDs threeDs;

		private ThreeDs getThreeDs() {
			return threeDs;
		}
	}

	private class ThreeDs {
		@SerializedName("result_code")
		private String resultCode;

		private String getResultCode() {
			return resultCode;
		}
	}
}
