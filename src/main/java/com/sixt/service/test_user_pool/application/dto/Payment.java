package com.sixt.service.test_user_pool.application.dto;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class Payment {
	private String id;
	private String token;
	private String paymentMethod;
	private String paymentSystem;

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the token
	 */
	public String getToken() {
		return token;
	}

	/**
	 * @param token the token to set
	 */
	public void setToken(String token) {
		this.token = token;
	}

	/**
	 * @return the paymentMethod
	 */
	public String getPaymentMethod() {
		return paymentMethod;
	}

	/**
	 * @param paymentMethod the paymentMethod to set
	 */
	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	/**
	 * @return the paymentSystem
	 */
	public String getPaymentSystem() {
		return paymentSystem;
	}

	/**
	 * @param paymentSystem the paymentSystem to set
	 */
	public void setPaymentSystem(String paymentSystem) {
		this.paymentSystem = paymentSystem;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this)
				.append("id", id)
				.append("token", token)
				.append("paymentMethod", paymentMethod)
				.append("paymentSystem", paymentSystem)
				.toString();
	}

}
