package com.sixt.service.test_user_pool.application;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.sixt.service.test_user_pool.application.dto.User;

import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import okhttp3.Headers;
import okhttp3.Headers.Builder;
import okhttp3.MediaType;

@Component
public class PersonApi {
	private static final Logger LOG = LoggerFactory.getLogger(PersonApi.class);
	private final MediaType JSON = MediaType.get("application/json; charset=utf-8");
	private Gson gson = new Gson();
	private HttpClientBase client = HttpClientBase.getInstance();

	private static final String X_API_KEY = "776ea3bf11df5829827f7afb43c37174";

	@Value("${person.url}")
	private String url;

	public PersonApi(
			@Value("${person.url}") final String url) {
			this.url = url;
			}

	/**
	 * @param url the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * Endpoint: /v1/persons/register
	 * Register a new user in Classic
	 * Headers: X-API-KEY
	 * Method: POST
	 */
	public User register() {
		final String path = "/v1/persons/register";
		final String emailProvider = "@test-mailhog.com";

		String randomStr = RandomStringUtils.randomAlphanumeric(7).toLowerCase();
		String randomEmailStr = System.currentTimeMillis() / 1000 + "-" + randomStr;
		String firstName = RandomStringUtils.randomAlphabetic(8).toUpperCase();
		String lastName = RandomStringUtils.randomAlphabetic(8).toUpperCase();

		String password = RandomStringUtils.randomAlphabetic(5).toLowerCase() //At least one lowercase.
			+ RandomStringUtils.randomAlphabetic(5).toUpperCase() // Minimum of 8 characters At least one lowercase.
			+ "1!"; // At least one digit At least one special character

		JsonObject payload = new JsonObject();
		payload.addProperty("firstName", firstName);
		payload.addProperty("lastName", lastName);
		payload.addProperty("email", randomEmailStr + emailProvider);
		payload.addProperty("password", password);


		Headers headers = new Builder()
			.set("X-API-KEY", X_API_KEY)
			.build();

		try {
			User user = gson.fromJson(client.post(url + path, payload.toString(), headers, JSON), User.class);
			if (user.getEmailAddress().contains(randomEmailStr)) {
				user.setPassword(password);
				LOG.info("Person <{}> registered in classic.", user.getUserId());
				return user;
			}
			LOG.error("Email address <{}> unknown.", user.getEmailAddress());
			return null;
		} catch (Exception e) {
			LOG.error("Failed to register a new user in classic. {}", e.getMessage());
			return null;
		}
	}
}
