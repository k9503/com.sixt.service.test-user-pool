package com.sixt.service.test_user_pool.application;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

@Component
public class ImageLoader {
	private static final Logger LOG = LoggerFactory.getLogger(ImageLoader.class);
	private static final String IMAGE_FILE_NAME = "test-image.png";
	private static File file;

	public ImageLoader(){
		LOG.info("Component ImageLoader init.");
	}

	public File any() {
		if (file == null) {
			InputStream ins = null;
			OutputStream outs = null;
			try {
				file = File.createTempFile("any", "png");
				ins = new ClassPathResource(IMAGE_FILE_NAME).getInputStream();
				outs = new FileOutputStream(file);

				byte[] buffer = new byte[1024];
				int len;
				while ((len=ins.read(buffer)) != -1) {
					outs.write(buffer, 0, len);
				}

				LOG.info("Image file <{}> created.", file);
			} catch (IOException ioe) {
				LOG.error("Load image <{}> failed. {}", IMAGE_FILE_NAME, ioe);
			} finally {
				try {
					ins.close();
					outs.close();
				} catch (Exception e) {
					LOG.warn("Failed closing input / output stream of imager loader.", e);
				}
			}
		} else {
			LOG.info("Reuse file <{}> for uploading.", file);
		}
		return file;
	}
}
