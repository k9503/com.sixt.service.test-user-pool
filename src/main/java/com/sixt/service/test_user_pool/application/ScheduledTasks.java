package com.sixt.service.test_user_pool.application;

import com.sixt.service.test_user_pool.api.UserType;
import com.sixt.service.test_user_pool.domain.TestUserJpaRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class ScheduledTasks {
	private static final Logger LOG = LoggerFactory.getLogger(ScheduledTasks.class);

	@Autowired
	private TestUserJpaRepository repo;

	@Autowired
	private TestUserFactory factory;

	/*
	 * fixedDelay - Execute the annotated method with a fixed period in milliseconds between the end of the last invocation and the start of the next.
	 * A scheduler which fills the pool of each user type with minimum ${MIN_POOL_SIZE} test users.
	 */
	@Scheduled(fixedDelay = 30000)
	public void fillPool() {
		final int MIN_POOL_SIZE = 500;
		final int WARN_LEVEL = MIN_POOL_SIZE >> 3; //Divide by 8. 12.5% of the MIN_POOL_SIZE is consider as warn level.
		long size;

		for (UserType type : UserType.values()) {
			// Ignore these types for the the moment
			if (type == UserType.UNRECOGNIZED || type == UserType.CLASSIC) {
				continue;
			}

			size = repo.countByUserTypeAndBlockedFalse(type);
			LOG.info("Available test user of type <{}>: {}", type, size);

			if (size <= WARN_LEVEL) {
				LOG.warn("The number of available test user of type {} is under warn level. {} remaining, {} expected.",
						type, size, MIN_POOL_SIZE);
			}

			if (size < MIN_POOL_SIZE) {
				final long numNewUsersToCreate = 3;

				int numCreatedTestUsers = factory.addTestUsers(type, numNewUsersToCreate);
				LOG.info("{} new test user(s) of type <{}> prepared.", numCreatedTestUsers, type);
			}
		}
	}

	/*
	 * Clean up all test-user from the pool which is unblocked.
	 * At 0:30AM every Saturday.
	 */
	@Scheduled(cron = "0 30 0 * * SAT")
	public void drainPool() {
		long amountToClean = repo.countByBlockedFalse();
		LOG.info("Cleaning up <{}> unused test user(s)...", amountToClean);

		long amountDeleted = repo.deleteByBlockedFalse();
		LOG.info("<{}> drained.", amountDeleted);
	}
}
