package com.sixt.service.test_user_pool.application;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.sixt.service.test_user_pool.application.dto.User;
import com.sixt.service.test_user_pool.domain.UserData;

import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import okhttp3.Headers;
import okhttp3.Headers.Builder;
import okhttp3.MediaType;

@Component
public class UsersApi {
	private static final Logger LOG = LoggerFactory.getLogger(UsersApi.class);
	private final MediaType JSON = MediaType.get("application/json; charset=utf-8");
	private Gson gson = new Gson();
	private HttpClientBase client = HttpClientBase.getInstance();

	// k8s deployment.yaml --> SPRING_PROFILES_ACTIVE: <dev | stage>
	// application-<SPRING_PROFILES>.yaml --> app.host
	@Value("${app.host}")
	private String host;

	public UsersApi(
			@Value("${app.host}") final String host) {
			this.host = host;
			}

	/**
	 * @param host the host to set
	 */
	public void setHost(String host) {
		this.host = host;
	}

	/**
	 * @return the host
	 */
	public String getHost() {
		return host;
	}

	public User register() {
		return register("de");
	}

	/**
	 * Endpoint: /v2/users
	 * Method: POST
	 * <p>
	 * Example response:
	 * {
	 * "userId": "46528309",
	 * "firstName": "2f52ff34",
	 * "lastName": "8d38e53b5071",
	 * "emailAddress": "2f52ff34-d39a-4a1c-b681-8d38e53b5071@test-mailhog.com",
	 * "isActivatedForRAC": false,
	 * "isActivatedForSAC": false,
	 * "previouslyActivatedForSAC": false,
	 * "sacInactiveReasons": null,
	 * "defaultLanguage": "en",
	 * "setAccountEmailAddressRequired": false
	 * }
	 */
	public User register(String language) {
		final String path = "/v2/users";
		final String defaultLanguage = "en";
		final String emailProvider = "@test-mailhog.com";

		String randomStr = RandomStringUtils.randomAlphanumeric(7).toLowerCase();
		String randomEmailStr = System.currentTimeMillis() / 1000 + "-" + randomStr;
		String firstName = RandomStringUtils.randomAlphabetic(8).toUpperCase();
		String lastName = RandomStringUtils.randomAlphabetic(8).toUpperCase();

		String password = RandomStringUtils.randomAlphabetic(5).toLowerCase() //At least one lowercase.
			+ RandomStringUtils.randomAlphabetic(5).toUpperCase() // Minimum of 8 characters At least one lowercase.
			+ "1!"; // At least one digit At least one special character

		JsonObject payload = new JsonObject();
		payload.addProperty("defaultLanguage", language.isEmpty() ? defaultLanguage : language);
		payload.addProperty("emailAddress", randomEmailStr + emailProvider);
		payload.addProperty("firstname", firstName);
		payload.addProperty("lastname", lastName);
		payload.addProperty("password", password);
		try {
			Headers headers = new Builder().build();
			User user = gson.fromJson(client.post(host + path, payload.toString(), headers, JSON), User.class);
			if (user.getEmailAddress().contains(randomEmailStr)) {
				user.setPassword(password);
				LOG.info("Person <{}> registered.", user.getUserId());
				return user;
			}
			LOG.error("Email address <{}> unknown.", user.getEmailAddress());
			return null;
		} catch (Exception e) {
			LOG.error("Failed to register new test user: {}", e.getMessage());
			return null;
		}
	}

	/**
	 * Endpoint: /v2/users/me/emailaddress/confirm
	 * Confirm the email:
	 * - Listen to event.PersonRegistered to get the confirmation hash
	 * - Request UsersApi `/me/emailaddress/confirm` endpoint with payload:
	 * {
	 * "confirmationHash": "string",
	 * "userId": "string"
	 * }
	 * <p>
	 * Method: POST
	 */
	public boolean confirmEmail(String userId, String hash) {

		final String path = "/v2/users/me/emailaddress/confirm";

		JsonObject payload = new JsonObject();
		payload.addProperty("confirmationHash", hash);
		payload.addProperty("userId", userId);

		try {
			Headers headers = new Builder().build();
			client.post(host + path, payload.toString(), headers, JSON);
			LOG.info("Confirming email for person <{}> hash <{}>.", userId, hash);
			return true;
		} catch (Exception e) {
			LOG.error("Failed to confirm email of person <{}>. {}", userId, e);
			return false;
		}
	}

	/**
	 * Endpoint: /v2/users/me/birthdate
	 * Method: PUT
	 */
	public boolean addBirthday(String token) {
		final String path = "/v2/users/me/birthdate";

		JsonObject payload = new JsonObject();
		payload.addProperty("dateOfBirth", "1977-01-01");

		try {
			Headers headers = new Builder()
				.set("Authorization", "Bearer " + token)
				.build();
			client.put(host + path, payload.toString(), headers, JSON);
		} catch (Exception e) {
			LOG.error("Failed to add birthday: {}", e.getMessage());
			return false;
		}
		return true;
	}

	/**
	 * Endpoint: /v2/users/me/homeAddress
	 * Method: PUT
	 */
	public boolean addHomeAddress(String token) {
		final String path = "/v2/users/me/homeAddress";

		JsonObject payload = new JsonObject();
		payload.addProperty("countryCode", "DE");
		payload.addProperty("addressee", "MÜLLER SCHMIDT");
		payload.addProperty("postalCode", "80000");
		payload.addProperty("locality", "MUNICH");
		payload.addProperty("thoroughfare", "HAUPTSTR.");
		payload.addProperty("thoroughfareNumber", "10");

		try {
			Headers headers = new Builder()
				.set("Authorization", "Bearer " + token)
				.build();
			client.put(host + path, payload.toString(), headers, JSON);
		} catch (Exception e) {
			LOG.error("Failed to add home address: {}", e.getMessage());
			return false;
		}
		return true;
	}

	/**
	 * Endpoint: /v2/users/me/homeAddress
	 * Method: PUT
	 */
	public boolean addItalyHomeAddress(String token) {
		final String path = "/v2/users/me/homeAddress";

		JsonObject payload = new JsonObject();
		payload.addProperty("countryCode", "IT");
		payload.addProperty("addressee", "ROSSI RUSSO");
		payload.addProperty("postalCode", "20129");
		payload.addProperty("locality", "MILANO");
		payload.addProperty("thoroughfare", "VIA MELZO");
		payload.addProperty("thoroughfareNumber", "28");

		try {
			Headers headers = new Builder()
				.set("Authorization", "Bearer " + token)
				.build();
			client.put(host + path, payload.toString(), headers, JSON);
		} catch (Exception e) {
			LOG.error("Failed to add home address: {}", e.getMessage());
			return false;
		}
		return true;
	}

	public boolean addAustriaHomeAddress(String token) {
		final String path = "/v2/users/me/homeAddress";

		JsonObject payload = new JsonObject();
		payload.addProperty("countryCode", "AT");
		payload.addProperty("addressee", "Franz Schubert");
		payload.addProperty("postalCode", "1030");
		payload.addProperty("locality", "Wien");
		payload.addProperty("thoroughfare", "Lothringerstraße");
		payload.addProperty("thoroughfareNumber", "20");

		try {
			Headers headers = new Builder()
				.set("Authorization", "Bearer " + token)
				.build();
			client.put(host + path, payload.toString(), headers, JSON);
		} catch (Exception e) {
			LOG.error("Failed to add home address: {}", e.getMessage());
			return false;
		}
		return true;
	}


	/**
	 * Endpoint: /v2/users/me/profiles/{profileId}/invoicingAddress
	 * Method: PUT
	 */
	public boolean changeProfileInvoicingAddress(String token, String profileId) {
		final String path = "/v2/users/me/profiles/" + profileId + "/invoicingAddress";

		JsonObject payload = new JsonObject();
		payload.addProperty("countryCode", "IT");
		payload.addProperty("addressee", "ROSSI RUSSO");
		payload.addProperty("postalCode", "20129");
		payload.addProperty("locality", "MILANO");
		payload.addProperty("thoroughfare", "VIA MELZO");
		payload.addProperty("thoroughfareNumber", "28");

		try {
			Headers headers = new Builder()
				.set("Authorization", "Bearer " + token)
				.build();
			client.put(host + path, payload.toString(), headers, JSON);
		} catch (Exception e) {
			LOG.error("Failed to change profile-{} address: {}", profileId, e.getMessage());
			return false;
		}
		return true;
	}

	/**
	 * Endpoint: /v2/users/me/passport
	 * Method: PUT
	 */
	public boolean addPassport(String token) {
		final String path = "/v2/users/me/passport";

		JsonObject payload = new JsonObject();
		payload.addProperty("passportNumber", "TEST000000");
		payload.addProperty("issuingCountry", "DE");
		payload.addProperty("issueDate", "1980-01-01");
		payload.addProperty("expirationDate", "2030-01-01");
		payload.addProperty("issuingAuthority", "TEST_AUTHORITY");

		try {
			Headers headers = new Builder()
				.set("Authorization", "Bearer " + token)
				.build();
			client.put(host + path, payload.toString(), headers, JSON);
		} catch (Exception e) {
			LOG.error("Failed to add passport: {}", e.getMessage());
			return false;
		}
		return true;
	}

	/**
	 * Endpoint: /v2/users/me/passport
	 * Method: PUT
	 */
	public boolean addItalyPassport(String token) {
		final String path = "/v2/users/me/passport";

		JsonObject payload = new JsonObject();
		payload.addProperty("passportNumber", "142356784");
		payload.addProperty("issuingCountry", "IT");
		payload.addProperty("issueDate", "1980-01-01");
		payload.addProperty("expirationDate", "2030-01-01");
		payload.addProperty("issuingAuthority", "MILANO");

		try {
			Headers headers = new Builder()
				.set("Authorization", "Bearer " + token)
				.build();
			client.put(host + path, payload.toString(), headers, JSON);
		} catch (Exception e) {
			LOG.error("Failed to add passport: {}", e.getMessage());
			return false;
		}
		return true;
	}

	public boolean addAustriaPassport(String token) {
		final String path = "/v2/users/me/passport";

		JsonObject payload = new JsonObject();
		payload.addProperty("passportNumber", "142356784");
		payload.addProperty("issuingCountry", "AT");
		payload.addProperty("issueDate", "1980-01-01");
		payload.addProperty("expirationDate", "2030-01-01");
		payload.addProperty("issuingAuthority", "WIEN");

		try {
			Headers headers = new Builder()
				.set("Authorization", "Bearer " + token)
				.build();
			client.put(host + path, payload.toString(), headers, JSON);
		} catch (Exception e) {
			LOG.error("Failed to add passport: {}", e.getMessage());
			return false;
		}
		return true;
	}

	/**
	 * Endpoint: /v2/users/me/driverslicense
	 * Method: PUT
	 */
	public boolean addDriversLicense(String token) {
		final String path = "/v2/users/me/driverslicense";

		JsonObject payload = new JsonObject();
		payload.addProperty("licenseNumber", "TEST_DL_" + RandomStringUtils.randomAlphabetic(12));
		payload.addProperty("drivingClass", "B");
		payload.addProperty("issuingCountry", "DE");
		payload.addProperty("issueDate", "2018-01-01");
		payload.addProperty("issuingAuthority", "TEST_AUTHORITY");
		payload.addProperty("allowedToDriveCarSinceDate", "2000-01-01");
		payload.addProperty("expirationDate", "2030-01-01");
		payload.addProperty("dateOfBirth", "1977-01-01");

		try {
			Headers headers = new Builder()
				.set("Authorization", "Bearer " + token)
				.build();
			client.put(host + path, payload.toString(), headers, JSON);
		} catch (Exception e) {
			LOG.error("Failed to add drivers license: {}", e.getMessage());
			return false;
		}
		return true;
	}

	/**
	 * Endpoint: /v2/users/me/driverslicense
	 * Method: PUT
	 */
	public boolean addItalyDriversLicense(String token) {
		final String path = "/v2/users/me/driverslicense";

		JsonObject payload = new JsonObject();
		payload.addProperty("licenseNumber", "TEST_DL_" + RandomStringUtils.randomAlphabetic(12));
		payload.addProperty("drivingClass", "B");
		payload.addProperty("issuingCountry", "IT");
		payload.addProperty("issueDate", "2018-01-01");
		payload.addProperty("issuingAuthority", "MILANO");
		payload.addProperty("allowedToDriveCarSinceDate", "2000-01-01");
		payload.addProperty("expirationDate", "2030-01-01");
		payload.addProperty("dateOfBirth", "1977-01-01");

		try {
			Headers headers = new Builder()
				.set("Authorization", "Bearer " + token)
				.build();
			client.put(host + path, payload.toString(), headers, JSON);
		} catch (Exception e) {
			LOG.error("Failed to add drivers license: {}", e.getMessage());
			return false;
		}
		return true;
	}

	/**
	 * Endpoint: /v2/users/me
	 * Method: GET
	 */
	public User getUser(String token) {
		final String path = "/v2/users/me";

		try {
			Headers headers = new Builder()
				.set("Authorization", "Bearer " + token)
				.build();
			return gson.fromJson(client.get(host + path, headers), User.class);
		} catch (Exception e) {
			LOG.error("Failed to get user data: {}", e.getMessage());
			return null;
		}
	}

	/**
	 * Endpoint: /v2/users/me/pin
	 * Method: POST
	 */
	public boolean setPin(String token) {
		final String path = "/v2/users/me/pin";

		JsonObject payload = new JsonObject();
		payload.addProperty("pin", UserData.PIN.DEFAULT);

		try {
			Headers headers = new Builder()
				.set("Authorization", "Bearer " + token)
				.build();
			client.post(host + path, payload.toString(), headers, JSON);
			return true;
		} catch (Exception e) {
			LOG.error("Failed to set pin: {}", e.getMessage());
			return false;
		}
	}

	/**
	 * Endpoint: /v2/users/me/phonenumber
	 * Method: PUT
	 */
	public boolean putPhoneNumber(String token) {
		final String path = "/v2/users/me/phonenumber";

		JsonObject payload = new JsonObject();
		payload.addProperty("countryCode", "+49");
		payload.addProperty("nationalNumber", "123456790");

		try {
			Headers headers = new Builder()
				.set("Authorization", "Bearer " + token)
				.build();
			client.put(host + path, payload.toString(), headers, JSON);
			return true;
		} catch (Exception e) {
			LOG.error("Failed to add phone number: {}", e.getMessage());
			return false;
		}
	}

	/**
	 * Endpoint: /v2/users/me/phonenumber
	 * Method: PUT
	 */
	public boolean putItalyPhoneNumber(String token) {
		final String path = "/v2/users/me/phonenumber";

		JsonObject payload = new JsonObject();
		payload.addProperty("countryCode", "+39");
		payload.addProperty("nationalNumber", "3457211247");

		try {
			Headers headers = new Builder()
				.set("Authorization", "Bearer " + token)
				.build();
			client.put(host + path, payload.toString(), headers, JSON);
			return true;
		} catch (Exception e) {
			LOG.error("Failed to add phone number: {}", e.getMessage());
			return false;
		}
	}

	public boolean putAustriaPhoneNumber(String token) {
		final String path = "/v2/users/me/phonenumber";

		JsonObject payload = new JsonObject();
		payload.addProperty("countryCode", "+43");
		payload.addProperty("nationalNumber", "123456789");

		try {
			Headers headers = new Builder()
				.set("Authorization", "Bearer " + token)
				.build();
			client.put(host + path, payload.toString(), headers, JSON);
			return true;
		} catch (Exception e) {
			LOG.error("Failed to add phone number: {}", e.getMessage());
			return false;
		}
	}
	/**
	 * Endpoint: /v2/users/me/profiles/{$profileId}/paymentInstrument
	 * Method: PUT
	 */
	public boolean changeProfilePayment(String token, String profileId, String paymentId) {
		final String path = "/v2/users/me/profiles/" + profileId + "/paymentInstrument";

		JsonObject payload = new JsonObject();
		payload.addProperty("paymentInstrumentId", paymentId);

		try {
			Headers headers = new Builder()
				.set("Authorization", "Bearer " + token)
				.build();
			client.put(host + path, payload.toString(), headers, JSON);
			return true;
		} catch (Exception e) {
			LOG.error("Failed to change profile payment: {}", e.getMessage());
			return false;
		}
	}
}
