package com.sixt.service.test_user_pool.application;

/**
 * OfficialLanguage is a map of country to its official language used.
 * As some countries have more regional languages, the official language can be found here:
 * https://en.wikipedia.org/wiki/List_of_official_languages_by_country_and_territory
 *
 * The language's isoCode is defined here:
 * https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes
 */
public enum OfficialLanguage {
	GERMANY("de"),
	AUSTRIA("de"),
	UNITED_STATES("en"),
	NETHERLANDS("nl"),
	ITALY("it"),
	DEFAULT("en");

	private String isoCode;

	OfficialLanguage(String isoCode) {
		this.isoCode = isoCode;
	}

	@Override
	public String toString(){
		return isoCode;
	}

	/**
	* @return the isoCode
	*/
	public String getIsoCode() {
		return isoCode;
	}
}
