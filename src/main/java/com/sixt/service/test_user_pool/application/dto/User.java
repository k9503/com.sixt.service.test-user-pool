package com.sixt.service.test_user_pool.application.dto;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.SerializedName;

public class User {
	private String userId;
	private String emailAddress;
	private String password;
	private String token;
	private String currentVersion;
	private String firstName;
	private String lastName;
	private String defaultLanguage;

	@SerializedName("profiles")
	private List<Profile> profiles;

	@SerializedName("paymentInstruments")
	private List<Payment> payments;

	public User() {
		userId = emailAddress = password = token = currentVersion = "";
		profiles = new ArrayList<>();
		payments = new ArrayList<>();
	}

	public User(String email, String pwd) {
		this.emailAddress = email;
		this.password = pwd;
	}

	public void mergeIfEmpty(User src) {
		if(this.userId.isEmpty()) userId = src.getUserId();
		if(this.emailAddress.isEmpty()) emailAddress = src.getEmailAddress();
		if(this.password.isEmpty()) password = src.getPassword();
		if(this.token.isEmpty()) token = src.getToken();
		if(this.currentVersion.isEmpty()) currentVersion = src.getCurrentVersion();
		if(this.profiles.isEmpty()) profiles = src.getProfiles();
		if(this.payments.isEmpty()) payments = src.getPayments();
		if(this.firstName.isEmpty()) firstName = src.getFirstName();
		if(this.lastName.isEmpty()) lastName = src.getLastName();
	}

	public Profile getPreselectedProfile() {
		for(Profile p :profiles) {
			if(p.isPreselected()) return p;
		}
		return null;
	}

	public Payment getAnyPayment() {
		for(Payment p : payments) {
			if(!p.getId().isEmpty()) return p;
		}
		return null;
	}

	/**
	 * @return the profiles
	 */
	public List<Profile> getProfiles() {
		return profiles;
	}

	/**
	 * @param profiles the profiles to set
	 */
	public void setProfiles(List<Profile> profiles) {
		this.profiles = profiles;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @param emailAddress the emailAddress to set
	 */
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	/**
	 * @return the emailAddress
	 */
	public String getEmailAddress() {
		return emailAddress;
	}

	/**
	 * @param token the token to set
	 */
	public void setToken(String token) {
		this.token = token;
	}

	/**
	 * @return the token
	 */
	public String getToken() {
		return token;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param currentVersion the currentVersion to set
	 */
	public void setCurrentVersion(String currentVersion) {
		this.currentVersion = currentVersion;
	}

	/**
	 * @return the currentVersion
	 */
	public String getCurrentVersion() {
		return currentVersion;
	}

	/**
	 * @return the payments
	 */
	public List<Payment> getPayments() {
		return payments;
	}

	/**
	 * @param payments the payments to set
	 */
	public void setPayments(List<Payment> payments) {
		this.payments = payments;
	}

	/**
	 *
	 * @return the first name
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 *
	 * @param firstName the first name
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 *
	 * @return the last name
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 *
	 * @param lastName the last name
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	* @return the defaultLanguage
	*/
	public String getDefaultLanguage() {
		return defaultLanguage;
	}

	/**
	* @param defaultLanguage the defaultLanguage to set
	*/
	public void setDefaultLanguage(String defaultLanguage) {
		this.defaultLanguage = defaultLanguage;
	}

	@Override
	public String toString() {
		return "User {\n" +
			"\tuserId='" + (userId==null?"":userId) + "\',\n" +
			"\temail address='" + (emailAddress==null?"":emailAddress) + "\',\n" +
			"\tpassword='" + (password==null?"":password.replaceAll(".", "*")) + "\',\n" +
			"\tcurrent version='" + (currentVersion==null?"":currentVersion) + "\',\n" +
			"\ttoken=<..." + (token==null?"0":token.length()) + " chars...>\n" +
			"\tprofiles=<..." + profiles.size() + " profile(s)...>\n" +
			"\tpayments=<..." + payments.size() + " payment(s)...>\n" +
			"\tfirstName='" + (firstName==null?"":firstName) + "\',\n" +
			"\tlastName='" + (lastName==null?"":lastName) + "\',\n" +
			"\tdefault language='" + (defaultLanguage==null?"":defaultLanguage) + "\',\n" +
			'}';
	}
}
