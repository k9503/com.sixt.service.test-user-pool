package com.sixt.service.test_user_pool.application;

import com.google.gson.Gson;

import com.google.gson.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import okhttp3.Headers;
import okhttp3.Headers.Builder;
import okhttp3.MediaType;

@Component
public class AuthApi {
	private static final Logger LOG = LoggerFactory.getLogger(AuthApi.class);
	private final MediaType JSON = MediaType.get("application/json; charset=utf-8");
	private Gson gson = new Gson();
	private HttpClientBase client = HttpClientBase.getInstance();

	// k8s deployment.yaml --> SPRING_PROFILES_ACTIVE: <dev | stage>
	// application-<SPRING_PROFILES>.yaml --> app.host
	@Value("${app.host}")
	private String host;

	public AuthApi(
			@Value("${app.host}") String host) {
			this.host = host;
			}

	/**
	 * @param host the host to set
	 */
	public void setHost(String host) {
		this.host = host;
	}

	/**
	 * @return the host
	 */
	public String getHost() {
		return host;
	}

	private String loginPayload(String email, String password) {
		JsonObject payload = new JsonObject();
		payload.addProperty("password", password);
		payload.addProperty("userName", email);

		return payload.toString();
	}

	/**
	 * Endpoint: /v1/auth/login
	 * Method: POST
	 */
	public String login(String email, String pwd) {
		final String PATH = "/v1/auth/login";
		String url = host + PATH;

		try {
			Headers headers = new Builder().build();
			String response = client.post(url, loginPayload(email, pwd), headers, JSON);
			LoginSuccessResponse loginSuccessResponse = gson.fromJson(response, LoginSuccessResponse.class);
			if (loginSuccessResponse.getAccessToken().length() > 100) {
				return loginSuccessResponse.getAccessToken();
			}
			LOG.error("Invalid token <{}>.", loginSuccessResponse.getAccessToken());
			return null;
		} catch (Exception e) {
			LOG.error("Failed login with email <{}>.", email);
			return null;
		}
	}

	private class LoginSuccessResponse {
		private String accessToken;

		private String getAccessToken() {
			return accessToken;
		}
	}
}
