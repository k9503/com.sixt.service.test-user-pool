package com.sixt.service.test_user_pool.application;

import static net.logstash.logback.marker.Markers.append;

import com.sixt.service.test_user_pool.api.UserType;
import com.sixt.service.test_user_pool.application.dto.User;
import com.sixt.service.test_user_pool.domain.TestUser;
import com.sixt.service.test_user_pool.domain.TestUserJpaRepository;
import com.sixt.service.test_user_pool.handler.EmailHandler;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import spring.boot.starter.sixtgoorange.copiedfromjamicro.Sleeper;

/**
 * A pseudo factory class for creating test user by calling APIs.
 */
@Component
public class TestUserFactory {
	private static final Logger LOG = LoggerFactory.getLogger(TestUserFactory.class);
	private Sleeper sleeper = new Sleeper();

	@Autowired
	private TestUserJpaRepository repo;

	@Autowired
	private PersonApi personApi;

	@Autowired
	private UsersApi usersApi;

	@Autowired
	private AuthApi authApi;

	@Autowired
	private PaymentApi paymentApi;

	@Autowired
	private DlcsApi dlcsApi;

	@Autowired
	private EmailHandler emailHandler;

	/**
	 * Create a defined number of fresh test users in the type given.
	 *
	 * @param type The user type to create.
	 * @param numUsersToAdd Expected number of new test users to be created
	 * @return Number of successfully created new test user which were also added to the DB
	 */
	public int addTestUsers(UserType type, long numUsersToAdd) {
		int numCreatedTestUsers = 0;

		for (int i = 0; i < numUsersToAdd; i++) {
			TestUser user = create(type);

			if (user != null && user.isValid()) {
				if (repo.save(user) != null) {
					numCreatedTestUsers++;
				}
			}
		}

		repo.flush();

		return numCreatedTestUsers;
	}

	private TestUser create(UserType type) {
		switch (type) {
			case CLASSIC:
				return createClassicUser();
			case VANILLA:
				return createVanillaUser();
			case FASTLANE:
				return createFastlaneUser();
			case SAC:
				return createShareACarUser(OfficialLanguage.DEFAULT);
			case SAC_EN:
				return createShareACarUser(OfficialLanguage.UNITED_STATES);
			case SAC_DE:
				return createShareACarUser(OfficialLanguage.GERMANY);
			case SAC_NL:
				return createShareACarUser(OfficialLanguage.NETHERLANDS);
			case SAC_IT:
				return createShareACarUser(OfficialLanguage.ITALY);
			case SAC_AT:
				return createShareACarUser(OfficialLanguage.AUSTRIA);
			default:
				LOG.warn("Skipped. Unknown type: <{}>.", type);
				return null;
		}
	}

	/**
	 * Not implemented yet.
	 *
	 * @return
	 */
	private TestUser createClassicUser() {
		//Register
		User user = doClassicRegister();
		if (user == null) return null;

		return new TestUser(user.getEmailAddress(), user.getPassword(), UserType.CLASSIC);
	}

	/**
	 * Return a Vanilla user or null.
	 *
	 * @see UserType.VANILLA
	 */
	private TestUser createVanillaUser() {
		//Register
		User user = doRegister(OfficialLanguage.UNITED_STATES);
		if (user == null) return null;

		//Confirm email
		if (!doConfirmEmail(user)) return null;

		return new TestUser(user.getEmailAddress(), user.getPassword(), UserType.VANILLA);
	}

	/**
	 * Return a Fastlane user or null.
	 *
	 * @see UserType.Fastlane
	 */
	private TestUser createFastlaneUser() {
		//Register
		User user = doRegister(OfficialLanguage.UNITED_STATES);
		if (user == null) return null;

		//First login
		if (!doLogin(user)) return null;

		//Add basic data
		if (!doAddPersonalData(user)) return null;

		//Confirm email
		if (!doConfirmEmail(user)) return null;

		//Add payment method
		if (!doAddPayment(user)) return null;

		//Add drivers license
		if (!doAddDriversLicense(user)) return null;

		return new TestUser(user.getEmailAddress(), user.getPassword(), UserType.FASTLANE);
	}

	/**
	 * Return a Share-A-Car user who has the default language set to @param language.
	 * The Italy user also has a dedicated home address and driverslicense.
	 *
	 * @see UserType.SAC
	 */
	private TestUser createShareACarUser(OfficialLanguage language) {
		//Register
		User user = doRegister(language);
		if (user == null) return null;

		//First login
		if (!doLogin(user)) return null;

		//Add basic data
		if (language == OfficialLanguage.ITALY) {
			if (!doAddItalyPersonalData(user)) return null;
		} else if (language == OfficialLanguage.AUSTRIA){
			if (!doAddAustriaPersonalData(user)) return null;
		} else {
			if (!doAddPersonalData(user)) return null;
		}

		//Confirm email
		if (!doConfirmEmail(user)) return null;

		//Add payment method
		if (!doAddPayment(user)) return null;

		//Add drivers license
		if (language == OfficialLanguage.ITALY) {
			if (!doAddItalyDriversLicense(user)) return null;
		} else {
			if (!doAddDriversLicense(user)) return null;
		}

		switch (language) {
			case GERMANY:
				return new TestUser(user.getEmailAddress(), user.getPassword(), UserType.SAC_DE);
			case NETHERLANDS:
				return new TestUser(user.getEmailAddress(), user.getPassword(), UserType.SAC_NL);
			case ITALY:
				return new TestUser(user.getEmailAddress(), user.getPassword(), UserType.SAC_IT);
			case AUSTRIA:
				return new TestUser(user.getEmailAddress(), user.getPassword(), UserType.SAC_AT);
			case UNITED_STATES:
				return new TestUser(user.getEmailAddress(), user.getPassword(), UserType.SAC_EN);
			default:
				return new TestUser(user.getEmailAddress(), user.getPassword(), UserType.SAC);
		}
	}

	private User doRegister(OfficialLanguage language) {
		LOG.info("Register a new user with random data.");
		User user = usersApi.register(language.getIsoCode());
		if (user == null) {
			return null;
		}

		String userId = user.getUserId();
		if (StringUtils.isBlank(userId)) {
			return null;
		}

		LOG.info(append("userId", user.getUserId()), "Registered user");

		return user;
	}

	private User doClassicRegister() {
		LOG.info("Register a new user with random data in classic system.");

		User user = personApi.register();
		if (user == null) {
			return null;
		}

		String userId = user.getUserId();
		if (StringUtils.isBlank(userId)) {
			return null;
		}

		LOG.info(append("userId", user.getUserId()), "Registered user");

		return user;
	}

	/**
	 * Login the user.
	 *
	 * @param user The user to login in.
	 * @return True when app token is not empty.
	 */
	private boolean doLogin(User user) {
		LOG.info(append("userId", user.getUserId()), "Login user");
		String token = authApi.login(user.getEmailAddress(), user.getPassword());
		user.setToken(token);

		return !StringUtils.isBlank(token);
	}

	/**
	 * Add personal data like birthday, home address, passport, phone number and pin.
	 *
	 * @param user The user to add basic data to.
	 * @return True when all data successfully added.
	 */
	private boolean doAddPersonalData(User user) {
		LOG.info(append("userId", user.getUserId()), "Add basic data to person ...");

		if (!usersApi.addBirthday(user.getToken())) {
			return false;
		}
		if (!usersApi.addHomeAddress(user.getToken())) {
			return false;
		}
		if (!usersApi.addPassport(user.getToken())) {
			return false;
		}
		if (!usersApi.putPhoneNumber(user.getToken())) {
			return false;
		}
		if (!usersApi.setPin(user.getToken())) {
			return false;
		}
		user.mergeIfEmpty(usersApi.getUser(user.getToken()));
		LOG.info(append("userId", user.getUserId()), "Basic data added");
		return true;
	}

	/**
	 * Add personal data like birthday, home address, passport, phone number and pin.
	 *
	 * @param user The user to add basic data to.
	 * @return True when all data successfully added.
	 */
	private boolean doAddItalyPersonalData(User user) {
		LOG.info("Add basic data to person <{}>...", user.getUserId());
		if (!usersApi.addBirthday(user.getToken())) {
			return false;
		}
		if (!usersApi.addItalyHomeAddress(user.getToken())) {
			return false;
		}
		if (!usersApi.addItalyPassport(user.getToken())) {
			return false;
		}
		if (!usersApi.putItalyPhoneNumber(user.getToken())) {
			return false;
		}
		if (!usersApi.setPin(user.getToken())) {
			return false;
		}
		user.mergeIfEmpty(usersApi.getUser(user.getToken()));
		LOG.info("Basic data added.");
		return true;
	}

	/**
	 * Add personal data like birthday, home address, passport, phone number and pin.
	 *
	 * @param user The user to add basic data to.
	 * @return True when all data successfully added.
	 */
	private boolean doAddAustriaPersonalData(User user) {
		LOG.info("Add basic data to person <{}>...", user.getUserId());
		if (!usersApi.addBirthday(user.getToken())) {
			return false;
		}
		if (!usersApi.addAustriaHomeAddress(user.getToken())) {
			return false;
		}
		if (!usersApi.addAustriaPassport(user.getToken())) {
			return false;
		}
		if (!usersApi.putAustriaPhoneNumber(user.getToken())) {
			return false;
		}
		if (!usersApi.setPin(user.getToken())) {
			return false;
		}
		user.mergeIfEmpty(usersApi.getUser(user.getToken()));
		LOG.info("Basic data added.");
		return true;
	}


	/**
	 * Confirm the email address of the user specified.
	 *
	 * @param user Confirm email for this user.
	 * @return True when the email get confirmed.
	 */
	private boolean doConfirmEmail(User user) {
		LOG.info(append("userId", user.getUserId()), "Confirm email of person ...");
		String hash = emailHandler.getConfirmationHash(user.getUserId());
		if (StringUtils.isBlank(hash)) {
			return false;
		}
		if (!usersApi.confirmEmail(user.getUserId(), hash)) {
			return false;
		}
		emailHandler.remove(user.getUserId());
		LOG.info(append("userId", user.getUserId()), "Email confirmed.");

		return true;
	}

	/**
	 * Add payment method to the user.
	 *
	 * @param user Add payment method to this user.
	 * @return True when a payment method has been added to the selected profile.
	 */
	private boolean doAddPayment(User user) {
		LOG.info(append("userId", user.getUserId()), "Add payment method for person ...");
		String signature = paymentApi.signature(user.getUserId(), user.getToken());
		if (StringUtils.isBlank(signature)) {
			return false;
		}

		if (!paymentApi.register(signature, user.getFirstName() + " " + user.getLastName()).equals("AUTHORIZED")) {
			return false;
		}

		boolean hasWallet = false;
		for (int i = 0; i < 10; i++) {
			try {
				Thread.sleep(1000);
				user.mergeIfEmpty(usersApi.getUser(user.getToken()));
				if (user.getAnyPayment() != null) {
					hasWallet = true;
					break;
				}
			} catch (InterruptedException e) {
				LOG.error("InterruptedException while checking for wallet: {}", e.getMessage());
			}
		}
		if (!hasWallet) {
			return false;
		}

		if (!usersApi.changeProfilePayment(user.getToken(), user.getPreselectedProfile().getId(), user.getAnyPayment().getId())) {
			return false;
		}
		LOG.info(append("userId", user.getUserId()), "Payment added");

		return true;
	}

	/**
	 * Add drivers license to the user. It takes at least 5 (sleeping) seconds.
	 *
	 * @param user Add drivers license to the user.
	 * @return True when the drivers license get reviewed and approved.
	 */
	private boolean doAddDriversLicense(User user) {
		LOG.info(append("userId", user.getUserId()), "Add drivers license for person ...");
		if (!usersApi.addDriversLicense(user.getToken())) {
			return false;
		}

		sleeper.sleepNoException(3000);

		if (!dlcsApi.attachAllFiles(user.getToken())) {
			return false;
		}

		String ticketId = dlcsApi.searchTicketId(user.getUserId());
		if (StringUtils.isBlank(ticketId)) {
			return false;
		}

		// Wait until the ticket ready to be reviewable.
		// https://jira.sixt.com/browse/RATS-3069
		// https://jira.sixt.com/browse/QAS-32
		sleeper.sleepNoException(3000);

		ticketId = dlcsApi.startReview(ticketId);
		if (!dlcsApi.approve(ticketId).equals("approved")) {
			return false;
		}
		LOG.info(append("userId", user.getUserId()), "Dlcs added");

		return true;
	}

	/**
	 * Add drivers license to the user.
	 *
	 * @param user Add drivers license to the user.
	 * @return True when the drivers license get reviewed and approved.
	 */
	private boolean doAddItalyDriversLicense(User user) {
		LOG.info("Add drivers license for person <{}>...", user.getUserId());
		if (!usersApi.addItalyDriversLicense(user.getToken())) {
			return false;
		}

		if (!dlcsApi.attachAllFiles(user.getToken())) {
			return false;
		}

		String ticketId = dlcsApi.searchTicketId(user.getUserId());
		if (StringUtils.isBlank(ticketId)) {
			return false;
		}

		ticketId = dlcsApi.startReview(ticketId);
		if (!dlcsApi.approve(ticketId).equals("approved")) {
			return false;
		}
		LOG.info("Dlcs added.");

		return true;
	}
}
