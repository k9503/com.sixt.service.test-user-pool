package com.sixt.service.test_user_pool.application;

import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import okhttp3.Headers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;

@Component
public class HttpClientBase {
	private static final Logger LOG = LoggerFactory.getLogger(HttpClientBase.class);
	private static HttpClientBase client;
	private OkHttpClient httpClient;

	private HttpClientBase() {}

	public static HttpClientBase getInstance() {
		if (client == null) {
			client = new HttpClientBase();
			client.httpClient = new OkHttpClient();
		}

		return client;
	}

	public String get(String url, Headers headers) throws Exception {
		Request request = new Request.Builder()
			.url(url)
			.headers(headers)
			.build();
		LOG.info("Request {}: {}", request.method(), request.url());

		Response response = httpClient.newCall(request).execute();

		LOG.info("Response code: <{}> {}", response.code(), response.message());
		if (!response.isSuccessful()) {
			throw new HttpRequestFailedException("Failed http GET request: " + response.code());
		}

		ResponseBody responseBody = response.body();
		if (responseBody != null) {
			String str = responseBody.string();
			responseBody.close();
			return str;
		} else {
			LOG.warn("Empty response body in GET response");
			return null;
		}
	}

	public String post(String url, String json, Headers headers, MediaType type) throws Exception {
		RequestBody body = RequestBody.create(json, type);

		Request request = new Request.Builder()
			.url(url)
			.headers(headers)
			.post(body)
			.build();
		LOG.info("Request {}: {}", request.method(), request.url());
		//LOG.info("Full request: {}", bodyToString(request.body()));

		Response response = httpClient.newCall(request).execute();

		LOG.info("Response code: <{}> {}", response.code(), response.message());

		ResponseBody responseBody = response.body();

		if (!response.isSuccessful()) {
			if (responseBody != null) {
				String str = responseBody.string();
				responseBody.close();
				LOG.warn("Response body: <{}>", str);
			}
			throw new HttpRequestFailedException("Failed http POST request: " + response.code());
		}

		if (responseBody != null) {
			String str = responseBody.string();
			responseBody.close();
			return str;
		} else {
			LOG.warn("Empty response body in POST response");
			return null;
		}
	}

	public String put(String url, String json, Headers headers, MediaType type) throws Exception {
		RequestBody body = RequestBody.create(json, type);

		Request request = new Request.Builder()
			.url(url)
			.headers(headers)
			.put(body)
			.build();
		LOG.info("Request {}: {}", request.method(), request.url());

		Response response = httpClient.newCall(request).execute();

		LOG.info("Response code: <{}> {}", response.code(), response.message());
		if (!response.isSuccessful()) {
			throw new HttpRequestFailedException("Failed http PUT request: " + response.code());
		}

		ResponseBody responseBody = response.body();
		if (responseBody != null) {
			String str = responseBody.string();
			responseBody.close();
			return str;
		} else {
			LOG.warn("Empty response body in PUT response");
			return null;
		}
	}

	public String postFile(String url, Headers headers, File file) throws Exception {
		RequestBody body = new MultipartBody.Builder()
			.setType(MultipartBody.FORM)
			.addFormDataPart("", null,
					RequestBody.create(file, MediaType.parse("image/png")))
			.build();

		Request request = new Request.Builder()
			.url(url)
			.headers(headers)
			.post(body)
			.build();
		LOG.info("Request {}: {}", request.method(), request.url());

		Response response = httpClient.newCall(request).execute();

		LOG.info("Response code: <{}> {}", response.code(), response.message());
		if (!response.isSuccessful()) {
			throw new HttpRequestFailedException("Failed http POST FILE request: " + response.code());
		}

		ResponseBody responseBody = response.body();
		if (responseBody != null) {
			String str = responseBody.string();
			responseBody.close();
			return str;
		} else {
			LOG.warn("Empty response body in POST FILE response");
			return null;
		}
	}
}
