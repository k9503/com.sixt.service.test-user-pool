package com.sixt.service.test_user_pool.application.dto;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class Profile {
	private String id;
	private String name;
	private boolean isPreselected;

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the isPreselected
	 */
	public boolean isPreselected() {
		return isPreselected;
	}

	/**
	 * @param isPreselected the isPreselected to set
	 */
	public void setPreselected(boolean isPreselected) {
		this.isPreselected = isPreselected;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this)
			.append("id", id)
			.append("name", name)
			.append("isPreselected", isPreselected)
			.toString();
	}

}
