package com.sixt.service.test_user_pool.application;

import java.lang.reflect.Type;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import okhttp3.Headers;
import okhttp3.Headers.Builder;
import okhttp3.MediaType;


@Component
public class DlcsApi {
	private static final Logger LOG = LoggerFactory.getLogger(DlcsApi.class);
	private final MediaType JSON = MediaType.get("application/json; charset=utf-8");
	private Gson gson = new Gson();
	private HttpClientBase client = HttpClientBase.getInstance();

	private final String xOperator = "E2E_TEST_DLCS_OPERATOR";
	private final String xApiKey = "ha0VDMR9QynaNyRtblBPpe4R";

	private ImageLoader loader = new ImageLoader();

	// k8s deployment.yaml --> SPRING_PROFILES_ACTIVE: <dev | stage>
	// application-<SPRING_PROFILES>.yaml --> app.host
	@Value("${app.host}")
	private String host;

	@Value("${dlcs.host}")
	private String dlcsHost;

	public DlcsApi(
			@Value("${app.host}") final String host,
			@Value("${dlcs.host}") final String dlcsHost) {
			this.host = host;
			this.dlcsHost = dlcsHost;
			}

	/**
	 * @param host the host to set
	 */
	public void setHost(String host) {
		this.host = host;
	}

	/**
	 * @return the host
	 */
	public String getHost() {
		return host;
	}

	/**
	 * @param dlcsHost the dlcsHost to set
	 */
	public void setDlcsHost(String dlcsHost) {
		this.dlcsHost = dlcsHost;
	}

	/**
	 * @return the dlcsHost
	 */
	public String getDlcsHost() {
		return dlcsHost;
	}

	/**
	 * Endpoint: /v1/dlcs/attach/{selfie | dl_front | dl_back | pass_front | pass_back}
	 * Method: POST
	 */
	public boolean attachAllFiles(String token) {
		final String[] types = {"selfie", "dl_front", "dl_back", "pass_front", "pass_back"};

		for (String type : types) {
			String url = host + "/v1/dlcs/attach/" + type;
			try {
				Headers headers = new Builder()
					.set("Authorization", "Bearer " + token)
					.build();
				UploadSuccessResponse response = gson.fromJson(client.postFile(url, headers, loader.any()), UploadSuccessResponse.class);
				LOG.info("Successfully uploaded {}. AssetID <{}>", type, response.getAssetID());
			} catch (Exception e) {
				LOG.error("Failed to upload file: {}.", e.getMessage());
				return false;
			}
		}
		return true;
	}

	/**
	 * Endpoint: /tickets/find?search={$PersonID}
	 * Method: GET
	 */
	public String searchTicketId(String personId) {
		final String url = dlcsHost + "/tickets/find?search=" + personId;

		try {
			Headers headers = new Builder()
				.set("X-Operator", xOperator)
				.set("X-Api-Key", xApiKey)
				.build();
			Type listType = new TypeToken<List<SearchSuccessResponse>>() {
			}.getType();
			List<SearchSuccessResponse> responseList = gson.fromJson(client.get(url, headers), listType);
			for (SearchSuccessResponse response : responseList) {
				LOG.info("Dlcs ticket ID: <{}>. Status: <{}>.", response.getTicketID(), response.getStatus());
			}

			if (responseList.size() >= 1) {
				return responseList.get(0).getTicketID();
			}
			return "";
		} catch (Exception e) {
			LOG.error("Failed to search DLCS ticket: {}", e.getMessage());
			return "";
		}
	}

	/**
	 * Endpoint: /tickets/${tid}/finishupload
	 * Method: POST
	 *
	 * Reference: https://jira.sixt.com/browse/RATS-3069
	 */
	public String finishUpload(String tid) {
		final String url = dlcsHost + "/tickets/" + tid + "/finishupload";

		try {
			Headers headers = new Builder()
				.set("X-Operator", xOperator)
				.set("X-Api-Key", xApiKey)
				.build();
			FinishUploadSuccessResponse response = gson.fromJson(client.post(url, "", headers, JSON), FinishUploadSuccessResponse.class);
			LOG.info("Upload dlcs ticket ID: <{}> finished.", response.getTicketID());
			return response.getTicketID();
		} catch (Exception e) {
			LOG.error("Failed to finish upload dlcs ticket: {}", e.getMessage());
			return "";
		}
	}

	/**
	 * Endpoint: /tickets/${tid}/startreview
	 * Method: POST
	 */
	public String startReview(String tid) {
		final String url = dlcsHost + "/tickets/" + tid + "/startreview";

		try {
			Headers headers = new Builder()
				.set("X-Operator", xOperator)
				.set("X-Api-Key", xApiKey)
				.build();
			StartReviewSuccessResponse response = gson.fromJson(client.post(url, "", headers, JSON), StartReviewSuccessResponse.class);
			LOG.info("Dlcs ticket ID: <{}>. Status: <{}>.", response.getTicketID(), response.getStatus());
			return response.getTicketID();
		} catch (Exception e) {
			LOG.error("Failed to review DLCS ticket: {}", e.getMessage());
			return "";
		}
	}

	/**
	 * Endpoint: /tickets/${tid}/approve
	 * Method: POST
	 */
	public String approve(String tid) {
		final String url = dlcsHost + "/tickets/" + tid + "/approve";

		try {
			Headers headers = new Builder()
				.set("X-Operator", xOperator)
				.set("X-Api-Key", xApiKey)
				.build();
			ApproveSuccessResponse response = gson.fromJson(client.post(url, "", headers, JSON), ApproveSuccessResponse.class);
			LOG.info("Dlcs ticket ID: <{}>. Status: <{}>.", response.getTicketID(), response.getStatus());
			return response.getStatus();
		} catch (Exception e) {
			LOG.error("Failed to approve DLCS ticket: {}", e.getMessage());
			return "";
		}
	}

	private class UploadSuccessResponse {
		private String assetID;

		private String getAssetID() {
			return assetID;
		}
	}

	private class SearchSuccessResponse {
		private String ticketID;
		private String status;

		private String getTicketID() {
			return ticketID;
		}

		private String getStatus() {
			return status;
		}
	}

	private class FinishUploadSuccessResponse {
		private String ticketID;

		private String getTicketID() {
			return ticketID;
		}
	}

	private class StartReviewSuccessResponse {
		private String ticketID;
		private String status;

		private String getTicketID() {
			return ticketID;
		}

		private String getStatus() {
			return status;
		}
	}

	private class ApproveSuccessResponse {
		private String ticketID;
		private String status;

		private String getTicketID() {
			return ticketID;
		}

		private String getStatus() {
			return status;
		}
	}
}
