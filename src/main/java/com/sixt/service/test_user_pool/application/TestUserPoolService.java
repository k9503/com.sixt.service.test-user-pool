package com.sixt.service.test_user_pool.application;

import java.time.Instant;
import java.util.Optional;

import com.sixt.service.test_user_pool.api.UserType;
import com.sixt.service.test_user_pool.domain.OutOfTestUserException;
import com.sixt.service.test_user_pool.domain.TestUser;
import com.sixt.service.test_user_pool.domain.TestUserJpaRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class TestUserPoolService {

	private static final Logger LOG = LoggerFactory.getLogger(TestUserPoolService.class);

	@Autowired
	private TestUserJpaRepository userRepository;

	@Autowired
	public TestUserPoolService(TestUserJpaRepository userRepository) {
		this.userRepository = userRepository;
		LOG.info("Starting TestUserPool service ...");
	}

	@Retryable(value = org.springframework.orm.ObjectOptimisticLockingFailureException.class)
	@Transactional
	public TestUser getUserFromPool(UserType type, String tcid) {
		TestUser user;

		try {
			user = userRepository.findFirstByUserTypeAndBlockedFalse(type);
			user.setTcId(tcid);
			user.setBlocked(true);
			user.setBlockedTime(Instant.now());
			userRepository.flush();
		} catch (Exception e) {
			LOG.error(e.getMessage());
			throw new OutOfTestUserException();
		}

		//This output is used to address the issue if a test user could be fetched in more than one time.
		LOG.info("Test user <{}> is consumed by {} at {}. ThreadID {}.", user.getName(), user.getTcId(), user.getBlockedTime(), Thread.currentThread().getId());

		return user;
	}

	public TestUser lookupUserFromPool(String name) {
		Optional<TestUser> opt = userRepository.findById(name);

		if(!opt.isPresent()) return null;
		return opt.get();
	}
}
