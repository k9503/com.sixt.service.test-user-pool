package com.sixt.service.test_user_pool.domain;

public class UndefinedUserTypeException extends RuntimeException {
	public static final String ERR_CODE = "Unrecognized user type.";

	public UndefinedUserTypeException() {
			super(ERR_CODE);
	}
}
