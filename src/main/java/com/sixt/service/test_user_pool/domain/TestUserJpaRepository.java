package com.sixt.service.test_user_pool.domain;

import com.sixt.service.test_user_pool.api.UserType;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

public interface TestUserJpaRepository extends JpaRepository<TestUser, String>{

	TestUser findFirstByUserTypeAndBlockedFalse(UserType type);
	long countByBlockedFalse();
	long countByUserTypeAndBlockedFalse(UserType type);

	@Transactional
	long deleteByBlockedFalse();
}
