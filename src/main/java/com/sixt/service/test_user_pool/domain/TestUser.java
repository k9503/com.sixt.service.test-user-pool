package com.sixt.service.test_user_pool.domain;

import java.time.Instant;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.sixt.service.test_user_pool.api.UserType;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Entity
@Table(name = "test_user", uniqueConstraints = {
	@UniqueConstraint(columnNames = {"name"})
})
public class TestUser {

	private static final Logger log = LoggerFactory.getLogger(TestUser.class);

	@Id
	@Column(unique = true)
	private String name;
	private String pwd;

	@Column(name = "user_type")
	@Enumerated(EnumType.STRING)
	private UserType userType;

	@Column(name = "tc_id", nullable = true)
	private String tcId;

	@Column(name = "creation_time")
	private Instant creationTime;

	@Column(nullable = false)
	private boolean blocked;

	@Column(name = "blocked_time", nullable = true)
	private Instant blockedTime;

	public TestUser() {}

	public TestUser(String name, String pwd) {
		this.name = name;
		this.pwd = pwd;
	}

	public TestUser(String name, String pwd, UserType type) {
		this.name = name;
		this.pwd = pwd;
		this.userType = type;
		this.creationTime = Instant.now();
	}

	public Instant getBlockedTime() {
		return blockedTime;
	}

	public void setBlockedTime(Instant blockedTime) {
		this.blockedTime = blockedTime;
	}

	public boolean isBlocked() {
		return blocked;
	}

	public void setBlocked(boolean blocked) {
		this.blocked = blocked;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public UserType getUserType() {
		return userType;
	}

	public void setUserType(UserType userType) {
		this.userType = userType;
	}

	public String getTcId() {
		return tcId;
	}

	public void setTcId(String tcId) {
		this.tcId = tcId;
	}

	public Instant getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(Instant creationTime) {
		this.creationTime = creationTime;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;

		if (o == null || getClass() != o.getClass()) return false;

		TestUser testUser = (TestUser) o;

		return new EqualsBuilder()
			.append(blocked, testUser.blocked)
			.append(name, testUser.name)
			.append(pwd, testUser.pwd)
			.append(userType, testUser.userType)
			.append(tcId, testUser.tcId)
			.append(creationTime, testUser.creationTime)
			.append(blockedTime, testUser.blockedTime)
			.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 37)
			.append(name)
			.append(pwd)
			.append(userType)
			.append(tcId)
			.append(creationTime)
			.append(blockedTime)
			.append(blocked)
			.toHashCode();
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this)
			.append("name", name)
			.append("pwd", pwd)
			.append("userType", userType)
			.append("tcId", tcId)
			.append("creationTime", creationTime)
			.append("blockedTime", blockedTime)
			.append("blocked", blocked)
			.toString();
	}

	public boolean isValid() {
		if (StringUtils.isBlank(name)) {
			log.warn("Invalid test user. Name is empty.");
			return false;
		}

		if (StringUtils.isBlank(pwd)) {
			log.warn("Invalid test user. Password is empty.");
			return false;
		}

		if (userType == null || userType == UserType.UNRECOGNIZED) {
			log.warn("Invalid test user. Unknown user type: <{}>.", userType);
			return false;
		}

		return true;
	}

}
