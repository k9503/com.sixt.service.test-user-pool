package com.sixt.service.test_user_pool.domain;

public class InvalidTestUserException extends RuntimeException {
	public static final String ERR_CODE = "Invalid test user data.";

	public InvalidTestUserException() {
			super(ERR_CODE);
	}
}
