package com.sixt.service.test_user_pool.domain;

public class TestUserNotFoundException extends RuntimeException {
	public static final String ERR_CODE = "Test user not found.";

	public TestUserNotFoundException() {
			super(ERR_CODE);
	}
}
