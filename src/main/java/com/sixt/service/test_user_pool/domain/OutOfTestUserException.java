package com.sixt.service.test_user_pool.domain;

public class OutOfTestUserException extends RuntimeException {
	public static final String ERR_CODE = "There is no available test user in the pool.";

	public OutOfTestUserException() {
		super(ERR_CODE);
	}
}
