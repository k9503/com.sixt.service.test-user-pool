package com.sixt.service.test_user_pool.domain;

public final class UserData {
	public final class PIN {
		public static final String DEFAULT = "0000";
	}

	public final class CC {
		public static final String BRAND = "MASTERCARD";
		public static final String EXPIRATION_MONTH = "03";
		public static final String EXPIRATION_YEAR = "2030";
		public static final String PAN = "5520117488551476";
		public static final String SEC_CODE = "737";
	}
}
