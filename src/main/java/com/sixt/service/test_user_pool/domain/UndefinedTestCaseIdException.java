package com.sixt.service.test_user_pool.domain;

public class UndefinedTestCaseIdException extends RuntimeException {
	public static final String ERR_CODE = "Undefined testcase id.";

	public UndefinedTestCaseIdException() {
			super(ERR_CODE);
	}
}
