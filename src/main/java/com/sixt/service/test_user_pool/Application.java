package com.sixt.service.test_user_pool;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class Application {

	public static void main(final String[] args) {
		new SpringApplicationBuilder()
			.sources(Application.class)
			.run(args);
	}
}
