package com.sixt.service.test_user_pool.handler;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class EmailHandler {
	private static final Logger LOG = LoggerFactory.getLogger(EmailHandler.class);

	private final static int MAX_CAPACITY = 100;

	/**
	 * A list of person IDs whose email need to be confirmed.
	 * Kay: PersonID
	 * Value: Email confirmation hash
	 */
	private Map<String, String> map;

	/**
	 * An helper queue to limited the size of map under MAX_CAPACITY.
	 */
	private Queue<String> pids;

	public EmailHandler() {
		map = new HashMap<>();
		pids = new LinkedList<>();
	}

	public void add(String pid, String hash) {
		if (map.size() > MAX_CAPACITY) {
			map.remove(pids.poll());
		}
		map.put(pid, hash);
		pids.offer(pid);
		LOG.info("Person <{}> is added to tobe confirmed list.", pid);
	}

	public String getConfirmationHash(String pid) {

		// Give some time for the <PersonRegistered> event to be consumed.
		for(int i=0; i<10; i++) {
			String hash = map.get(pid);
			if(hash == null || hash.isEmpty()) {
				try {
					Thread.sleep(500);
				} catch (InterruptedException e){
					e.printStackTrace();
				}
				continue;
			}
			return hash;
		}

		LOG.warn("Could not find confirmation hash for <{}>.", pid);
		return "";
	}

	public void remove(String pid) {
		map.remove(pid);
		pids.remove(pid);
	}
}
