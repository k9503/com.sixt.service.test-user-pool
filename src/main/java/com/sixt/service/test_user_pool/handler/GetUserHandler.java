package com.sixt.service.test_user_pool.handler;

import com.sixt.lib.rpcclient.RpcCallException.Category;
import com.sixt.service.test_user_pool.api.GetUserRequest;
import com.sixt.service.test_user_pool.api.GetUserResponse;
import com.sixt.service.test_user_pool.api.UserType;
import com.sixt.service.test_user_pool.application.TestUserPoolService;
import com.sixt.service.test_user_pool.domain.OutOfTestUserException;
import com.sixt.service.test_user_pool.domain.TestUser;
import com.sixt.service.test_user_pool.domain.UndefinedTestCaseIdException;
import com.sixt.service.test_user_pool.domain.UndefinedUserTypeException;
import com.sixt.service.test_user_pool.domain.UserData;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import spring.boot.starter.sixtgoorange.copiedfromjamicro.OrangeContext;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.RpcCallException;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.ServiceMethodHandler;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.webservice.RpcHandler;

@Component
@RpcHandler("TestUserPool.GetUser")
public class GetUserHandler extends ExceptionMappingHandler implements ServiceMethodHandler<GetUserRequest, GetUserResponse> {

	private static final Logger LOG = LoggerFactory.getLogger(GetUserHandler.class);

	@Autowired
	private TestUserPoolService service;

	@Autowired
	public GetUserHandler() {
		//Mapping proto exception to rpc exception
		register(UndefinedUserTypeException.class, RpcCallException.Category.BadRequest, UndefinedUserTypeException.ERR_CODE);
		register(UndefinedTestCaseIdException.class, RpcCallException.Category.BadRequest, UndefinedTestCaseIdException.ERR_CODE);
		register(OutOfTestUserException.class, RpcCallException.Category.ResourceNotFound, OutOfTestUserException.ERR_CODE);
	}

	@Override
	public GetUserResponse handleRequest(GetUserRequest request, OrangeContext orangeContext) throws RpcCallException {

		LOG.info("Handling GetUserRequest...");

		try {
			validate(request);
		} catch (Exception e) {
			LOG.error("Validation of GetUserRequest failed: {}", e.getMessage());
			throw mapException(e);
		}

		TestUser testUser;
		try {
			testUser = service.getUserFromPool(request.getUserType(), request.getTcId());
		} catch (Exception e) {
			LOG.error("Failed getting test user: {}", e.getMessage());
			throw mapException(e);
		}

		return GetUserResponse.newBuilder()
			.setName(testUser.getName())
			.setPwd(testUser.getPwd())
			.setPin(UserData.PIN.DEFAULT)
			.setCc(UserData.CC.PAN)
			.build();
	}

	private void validate(GetUserRequest getUserRequest) throws
		UndefinedUserTypeException,
		UndefinedTestCaseIdException,
		RpcCallException {
			if (getUserRequest == null) {
				throw new RpcCallException(Category.BadRequest, "Invalid request: null.");
			}
			if (getUserRequest.getUserType() == null) {
				throw new UndefinedUserTypeException();
			}
			if (UserType.forNumber(getUserRequest.getUserTypeValue()) == null) {
				throw new UndefinedUserTypeException();
			}
			if (getUserRequest.getTcId() == null || getUserRequest.getTcId().isEmpty()) {
				throw new UndefinedTestCaseIdException();
			}
		}
}
