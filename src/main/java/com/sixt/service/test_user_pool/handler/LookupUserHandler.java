package com.sixt.service.test_user_pool.handler;

import com.sixt.service.test_user_pool.api.LookupUserRequest;
import com.sixt.service.test_user_pool.api.LookupUserResponse;
import com.sixt.service.test_user_pool.application.TestUserPoolService;
import com.sixt.service.test_user_pool.domain.TestUser;
import com.sixt.service.test_user_pool.domain.TestUserNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.OrangeContext;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.ServiceMethodHandler;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.webservice.RpcHandler;

import static net.logstash.logback.marker.Markers.append;

@RpcHandler("TestUserPool.LookupUser")
public class LookupUserHandler
	implements ServiceMethodHandler<LookupUserRequest, LookupUserResponse> {

	private static final Logger LOG = LoggerFactory.getLogger(LookupUserHandler.class);

	@Autowired
	private TestUserPoolService service;

	@Override
	public LookupUserResponse handleRequest(LookupUserRequest request, OrangeContext orangeContext) {

		LOG.info(append("name", request.getName()), "Handling LookupUser");

		TestUser testUser = service.lookupUserFromPool(request.getName());

		if (testUser == null) {
			throw new TestUserNotFoundException();
		}

		return LookupUserResponse.newBuilder()
			.setUserType(testUser.getUserType())
			.setPwd(testUser.getPwd())
			.setTcId(testUser.getTcId()).build();
	}

}
