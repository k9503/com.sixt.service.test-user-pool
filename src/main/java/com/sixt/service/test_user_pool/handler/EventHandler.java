package com.sixt.service.test_user_pool.handler;

import javax.annotation.Nonnull;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
public class EventHandler {
	private static final Logger LOG = LoggerFactory.getLogger(EventHandler.class);

	@Autowired
	EmailHandler emailHandler;

	@Autowired
	Gson gson;

	/**
	 * Example {@link com.sixt.lib.event_schema.PersonRegisteredOuterClass.PersonRegistered} payload in JSON:
	 * {
	 * ConsumerRecord(
	 * 	topic = events.PersonRegistered,
	 * 	key = 36697798,
	 * 	value = {
	 * 		"meta": {
	 * 			"name": "PersonRegistered",
	 * 			"timestamp": "2019-08-22T08:10:14.282Z",
	 * 			"correlation_id": "8646acbd-084e-4a9b-b76d-8e97c20614f6",
	 * 			"grouping": "Person",
	 * 			"distribution_key": "36697798"
	 * 		},
	 * 		"person_id": "36697798",
	 * 		"given_name": "349500bc",
	 * 		"family_name": "1d215d632d30",
	 * 		"email_address": "349500bc-b219-451a-bde4-1d215d632d30@test-mailhog.com",
	 * 		"confirmation_hash": "8a30c57f925f0811764b6363afa75a89",
	 * 		"exists_in_partner": true,
	 * 		"default_language": "en",
	 * 		"channel_name": "web-next"
	 * 	})",
	 * }
	 */
	@KafkaListener(topics = "events.PersonRegistered", groupId = "test_user_pool")
	public void PersonRegisteredListener(@Nonnull final ConsumerRecord<String, String> customerRecord) {
		try {
			SuccessfulRegistration successfulRegistration = gson.fromJson(customerRecord.value(), SuccessfulRegistration.class);
			emailHandler.add(successfulRegistration.getPersonId(), successfulRegistration.getConfirmationHash());
		} catch (final Exception e) {
			LOG.warn("Can not get event from topic {}", customerRecord.topic(), e);
		}
	}

	private class SuccessfulRegistration {
		@SerializedName("person_id")
		private String personId;

		@SerializedName("confirmation_hash")
		private String confirmationHash;

		private String getPersonId() {
			return personId;
		}

		private String getConfirmationHash() {
			return confirmationHash;
		}
	}
}
