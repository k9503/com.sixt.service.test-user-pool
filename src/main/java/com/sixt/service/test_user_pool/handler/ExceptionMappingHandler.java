package com.sixt.service.test_user_pool.handler;

import java.util.AbstractMap;
import java.util.HashMap;
import java.util.Map;

import spring.boot.starter.sixtgoorange.copiedfromjamicro.RpcCallException;

public class ExceptionMappingHandler {

	private Map<Class<? extends Exception>, Map.Entry<RpcCallException.Category, String>> exceptionMap = new HashMap<>();
	private RpcCallException fallbackException;

	protected void register(Class<? extends Exception> exceptionClass, RpcCallException.Category category, String errorCode) {
		this.exceptionMap.put(exceptionClass, new AbstractMap.SimpleEntry<>(category, errorCode));
	}

	protected RpcCallException mapException(Exception e) {
		if (e instanceof RpcCallException) {
			return (RpcCallException) e;
		}

		//check exceptionMap for mappings
		Map.Entry<RpcCallException.Category, String> mapEntry = exceptionMap.get(e.getClass());
		if (mapEntry != null) {
			return new RpcCallException(mapEntry.getKey(), e.getMessage())
				.withErrorCode(mapEntry.getValue());
		}

		return fallbackException;
	}

	protected void setFallbackException(RpcCallException e) {
		fallbackException = e;
	}
}
