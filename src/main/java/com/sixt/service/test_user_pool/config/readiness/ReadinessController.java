package com.sixt.service.test_user_pool.config.readiness;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public final class ReadinessController {

    @RequestMapping("/readiness")
    public ResponseEntity<Void> readiness() {
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
