package com.sixt.service.test_user_pool;

import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.testcontainers.containers.PostgreSQLContainer;

import javax.annotation.Nonnull;

	@RunWith(SpringRunner.class)
	@Category(IntegrationTest.class)
	@SpringBootTest
	@ActiveProfiles("test")
	@ContextConfiguration(initializers = {AbstractIntegrationTest.Initializer.class})
	public abstract class AbstractIntegrationTest {

		private static final PostgreSQLContainer postgres = new PostgreSQLContainer();

		static {
			postgres.start();
		}

		static class Initializer
				implements ApplicationContextInitializer<ConfigurableApplicationContext> {

				@Override
				public void initialize(@Nonnull final ConfigurableApplicationContext configurableApplicationContext) {
					TestPropertyValues.of(
							"spring.datasource.url=" + postgres.getJdbcUrl(),
							"spring.datasource.username=" + postgres.getUsername(),
							"spring.datasource.password=" + postgres.getPassword()
							).applyTo(configurableApplicationContext.getEnvironment());
				}
		}
	}
