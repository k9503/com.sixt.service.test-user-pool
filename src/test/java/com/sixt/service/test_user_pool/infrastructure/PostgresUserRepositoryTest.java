package com.sixt.service.test_user_pool.infrastructure;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.fail;

import java.time.Instant;
import java.util.Optional;
import java.util.Random;
import java.util.UUID;

import com.sixt.service.test_user_pool.api.UserType;
import com.sixt.service.test_user_pool.domain.TestUser;
import com.sixt.service.test_user_pool.domain.TestUserJpaRepository;

import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.testcontainers.containers.PostgreSQLContainer;

@RunWith(SpringRunner.class)
@DataJpaTest()
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ActiveProfiles({"test"})
@ContextConfiguration(initializers = {PostgresUserRepositoryTest.Initializer.class})
public class PostgresUserRepositoryTest {

	@ClassRule
	public static PostgreSQLContainer postgreSQLContainer = new PostgreSQLContainer();

	static class Initializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {
		@Override
		public void initialize(ConfigurableApplicationContext applicationContext) {
			TestPropertyValues.of(
					"spring.datasource.url=" + postgreSQLContainer.getJdbcUrl(),
					"spring.datasource.username=" + postgreSQLContainer.getUsername(),
					"spring.datasource.password=" + postgreSQLContainer.getPassword()
					).applyTo(applicationContext.getEnvironment());
		}
	}

	@Autowired
	private TestUserJpaRepository userRepository;

	static private String NAME, PASSWORD;
	static private UserType TYPE;

	@Before
	public void beforeTest() {
		userRepository.deleteAll();

		NAME = PASSWORD = UUID.randomUUID().toString();
		TYPE = randomType();
	}

	private UserType randomType() {
		final UserType[] availableTypes = {UserType.VANILLA, UserType.SAC, UserType.FASTLANE};

		return availableTypes[new Random().nextInt(availableTypes.length)];
	}

	@Test
	public void create_success() {
		userRepository.saveAndFlush(new TestUser(NAME, PASSWORD, TYPE));

		Optional<TestUser> optional = userRepository.findById(NAME);
		if(!optional.isPresent()) {
			fail("Couldn't find user.");
		}

		TestUser foundUser = optional.get();
		assertThat(foundUser.getName()).isEqualTo(NAME);
		assertThat(foundUser.getPwd()).isEqualTo(PASSWORD);
		assertThat(foundUser.getUserType()).isEqualTo(TYPE);
		assertThat(foundUser.getTcId()).isNullOrEmpty();
		assertThat(foundUser.getCreationTime()).isNotNull();
		assertThat(foundUser.isBlocked()).isFalse();
		assertThat(foundUser.getBlockedTime()).isNull();
	}

	@Test
	public void create_ignore_InvalidTestUser() {
		try {
			userRepository.saveAndFlush(new TestUser());
			fail("Expected to have JPA system exception, but not.");
		} catch (org.springframework.orm.jpa.JpaSystemException se) {
			final String ID_NOT_SET = "ids for this class must be manually assigned before calling save()";
			assertThat(se.getMessage()).containsIgnoringCase(ID_NOT_SET);
		} catch (Exception e) {
			fail("Unexpected exception." + e);
		}
	}

	@Test
	public void getByType_success() {
		userRepository.saveAndFlush(new TestUser(NAME, PASSWORD, TYPE));

		TestUser user = userRepository.findFirstByUserTypeAndBlockedFalse(TYPE);
		assertThat(user.getUserType()).isEqualTo(TYPE);
		assertThat(user.getName()).isEqualToIgnoringCase(NAME);
		assertThat(user.getCreationTime()).isNotNull();
	}

	@Test
	public void getByName_success() {
		userRepository.saveAndFlush(new TestUser(NAME, PASSWORD, TYPE));

		Optional<TestUser> optional = userRepository.findById(NAME);

		assertThat(optional.isPresent()).isTrue();

		TestUser user = optional.get();
		assertThat(user.getName()).isEqualTo(NAME);
		assertThat(user.getPwd()).isEqualTo(PASSWORD);
		assertThat(user.getUserType()).isEqualTo(TYPE);
		assertThat(user.getCreationTime()).isNotNull();
	}

	@Test
	public void update_success() {
		TestUser user = new TestUser(NAME, PASSWORD, TYPE);

		try {
			user = userRepository.saveAndFlush(user);
		} catch (Exception e) {
			fail(e.getMessage());
		}
		assertThat(user.getName()).isEqualTo(NAME);
		assertThat(user.getPwd()).isEqualTo(PASSWORD);
		assertThat(user.getUserType()).isEqualTo(TYPE);
		assertThat(user.getTcId()).isNullOrEmpty();
		assertThat(user.getCreationTime()).isNotNull();
		assertThat(user.isBlocked()).isFalse();
		assertThat(user.getBlockedTime()).isNull();

		final String tcid = this.getClass().getName() + "_update_success";
		user.setTcId(tcid);
		user.setBlocked(true);
		user.setBlockedTime(Instant.now());

		userRepository.flush();

		TestUser got = new TestUser();
		try {
			got = userRepository.findById(user.getName()).get();
		} catch (Exception e) {
			fail(e.getMessage());
		}
		assertThat(got.getTcId()).isEqualTo(tcid);
		assertThat(got.isBlocked()).isTrue();
		assertThat(got.getBlockedTime()).isNotNull();
	}

	@Test
	public void delete_success() {
		TestUser user = new TestUser();
		try {
			user = userRepository.saveAndFlush(new TestUser(NAME, PASSWORD, TYPE));
		} catch (Exception e) {
			fail(e.getMessage());
		}

		assertThat(user.getName()).isEqualTo(NAME);

		userRepository.deleteById(NAME);
		assertThat(userRepository.findById(NAME).isPresent()).isFalse();
	}

	@Test
	public void getAll_success() {
		final int ALL = 10;
		for (int i = 0; i < ALL; i++) {
			userRepository.save(new TestUser(UUID.randomUUID().toString(), PASSWORD, TYPE));
		}
		userRepository.flush();

		assertThat(userRepository.findAll().size()).isEqualTo(ALL);
	}

	@Test
	public void getAvailableAmountOfType_success() {
		int maxAmount = 5;
		for (int i = 0; i < maxAmount; i++) {
			userRepository.save(new TestUser(UUID.randomUUID().toString(), PASSWORD, TYPE));
		}

		long amount = userRepository.countByUserTypeAndBlockedFalse(TYPE);
		assertThat(amount).isEqualTo(maxAmount);
	}
}
