package com.sixt.service.test_user_pool;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import com.sixt.service.test_user_pool.api.UserType;
import com.sixt.service.test_user_pool.application.AuthApi;
import com.sixt.service.test_user_pool.application.DlcsApi;
import com.sixt.service.test_user_pool.application.OfficialLanguage;
import com.sixt.service.test_user_pool.application.PaymentApi;
import com.sixt.service.test_user_pool.application.UsersApi;
import com.sixt.service.test_user_pool.application.dto.User;

import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Ignore
public class RegisterTest {
	private static UsersApi usersApi;
	private static AuthApi authApi;
	private static DlcsApi dlcsApi;
	private static PaymentApi paymentApi;
	private static final Logger LOG = LoggerFactory.getLogger(RegisterTest.class);

	private final static String devHost = "https://e2e-test-api-dev.goorange.sixt.com";
	private final static String stageHost = "https://e2e-test-api-stage.goorange.sixt.com";

	private final static String devDlcsHost = "http://dlcs-acceptance.sixt.com";
	private final static String stageDlcsHost = "http://dlcs-test.sixt.com";

	private final static String devPaymentHost = "https://api.dev.sixt-payment.com";
	private final static String stagePaymentHost = "https://api.stage.sixt-payment.com";

	/*
	* Uncomment following line to test on dev
	*/
	private final static String host = stageHost;
	private final static String dlcsHost = stageDlcsHost;
	private final static String paymentHost = stagePaymentHost;

	/*
	* Uncomment following line to test on stage
	*/
	//private final static String host = stageHost;
	//private final static String dlcsHost = stageDlcsHost;
	//private final static String paymentHost = stagePaymentHost;

	@BeforeClass
	public static void setUp() {
		usersApi = new UsersApi(host);
		authApi = new AuthApi(host);
		dlcsApi = new DlcsApi(host, dlcsHost);
		paymentApi = new PaymentApi(host, paymentHost);
	}

	@Test
	public void registerTest() {
		UserType type = UserType.SAC;
		User user = usersApi.register();
		assertNotNull(user);

		String token = authApi.login(user.getEmailAddress(), user.getPassword());
		assertNotNull(token);

		user.setToken(token);
		LOG.info("New <{}> user created.\n User: {}", type, user);
	}

	@Test
	public void registerTest_Italy() {
		final String DEFAULT_LANGUAGE_IT = "IT";

		UserType type = UserType.SAC_IT;
		User user = usersApi.register(DEFAULT_LANGUAGE_IT);
		LOG.info("Test user registered: {}", user);
		assertNotNull(user);

		String token = authApi.login(user.getEmailAddress(), user.getPassword());
		assertNotNull(token);

		user.setToken(token);
		LOG.info("New <{}> user created.\n User: {}", type, user);

		assertThat(usersApi.getUser(token).getDefaultLanguage()).isEqualToIgnoringCase(DEFAULT_LANGUAGE_IT);
	}

	@Test
	public void registerTest_Austria() {
		UserType type = UserType.SAC_AT;
		User user = usersApi.register(OfficialLanguage.AUSTRIA.getIsoCode());
		LOG.info("Test user registered: {}", user);
		assertNotNull(user);

		String token = authApi.login(user.getEmailAddress(), user.getPassword());
		assertNotNull(token);

		user.setToken(token);
		LOG.info("New <{}> user created.\n User: {}", type, user);

		assertThat(usersApi.getUser(token).getDefaultLanguage()).isEqualToIgnoringCase(OfficialLanguage.AUSTRIA.getIsoCode());
	}

	@Test
	public void addBirthdayTest() {
		User user = usersApi.register();
		LOG.info("Test user registered: {}", user);

		String token = authApi.login(user.getEmailAddress(), user.getPassword());
		user.setToken(token);

		assertTrue(usersApi.addBirthday(user.getToken()));
	}

	@Test
	public void addHomeAddressTest() {
		User user = usersApi.register();
		String token = authApi.login(user.getEmailAddress(), user.getPassword());
		user.setToken(token);
		assertTrue(usersApi.addHomeAddress(user.getToken()));
	}

	@Test
	public void addPassportTest() {
		User user = usersApi.register();
		String token = authApi.login(user.getEmailAddress(), user.getPassword());
		user.setToken(token);
		assertTrue(usersApi.addPassport(user.getToken()));
	}

	@Test
	public void addDriversLicenseTest() {
		User user = usersApi.register();
		String token = authApi.login(user.getEmailAddress(), user.getPassword());
		user.setToken(token);

		assertTrue(usersApi.addBirthday(user.getToken()));
		assertTrue(usersApi.addPassport(user.getToken()));
		assertTrue(usersApi.addDriversLicense(user.getToken()));

		dlcsApi.attachAllFiles(user.getToken());

		String tid = dlcsApi.searchTicketId(user.getUserId());
		assertFalse(tid.isEmpty());

		try {
			// 28.04.2020:
			// Tested with 2 seconds, but still get http 403 from time to time.
			// So I have to use 3 seconds here.
			Thread.sleep(3000);
		} catch (InterruptedException e){
			e.printStackTrace();
		}

		tid = dlcsApi.startReview(tid);
		assertFalse(tid.isEmpty());
		assertThat(dlcsApi.approve(tid)).isEqualTo("approved");
	}

	@Test
	public void addPinTest() {
		User user = usersApi.register();
		String token = authApi.login(user.getEmailAddress(), user.getPassword());
		user.setToken(token);
		usersApi.setPin(user.getToken());
	}

	@Test
	public void addPhoneNumberTest() {
		User user = usersApi.register();
		String token = authApi.login(user.getEmailAddress(), user.getPassword());
		user.setToken(token);
		usersApi.putPhoneNumber(user.getToken());
	}

	@Test
	public void getPaymentSignatureTest() {
		User user = usersApi.register();
		String token = authApi.login(user.getEmailAddress(), user.getPassword());
		user.setToken(token);

		String sig = paymentApi.signature(user.getUserId(), user.getToken());
		assertFalse(sig.isEmpty());
	}

	@Test
	public void addPaymentTest() {
		User user = usersApi.register();
		String token = authApi.login(user.getEmailAddress(), user.getPassword());
		user.setToken(token);

		String sig = paymentApi.signature(user.getUserId(), user.getToken());
		assertThat(paymentApi.register(sig, user.getFirstName() + " " + user.getLastName())).isEqualTo("AUTHORIZED");

		user.mergeIfEmpty(usersApi.getUser(user.getToken()));
	}

	@Test
	public void createShareACarUserTest() {
		//Register
		User user = usersApi.register();
		String token = authApi.login(user.getEmailAddress(), user.getPassword());
		user.setToken(token);

		//Add basic data
		assertTrue(usersApi.addBirthday(user.getToken()));
		assertTrue(usersApi.addPassport(user.getToken()));
		assertTrue(usersApi.addHomeAddress(user.getToken()));
		assertTrue(usersApi.putPhoneNumber(user.getToken()));
		assertTrue(usersApi.setPin(user.getToken()));

		//Add payment method
		String sig = paymentApi.signature(user.getUserId(), user.getToken());
		assertThat(paymentApi.register(sig, user.getFirstName() + " " + user.getLastName())).isEqualTo("AUTHORIZED");

		boolean hasWallet = false;
		for (int i = 0; i < 10; i++) {
			try {
				Thread.sleep(1000);
				user.mergeIfEmpty(usersApi.getUser(user.getToken()));
				if (user.getAnyPayment() != null) {
					hasWallet = true;
					break;
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		assertTrue(hasWallet);
		usersApi.changeProfilePayment(user.getToken(), user.getPreselectedProfile().getId(), user.getAnyPayment().getId());

		//Add drivers license
		assertTrue(usersApi.addDriversLicense(user.getToken()));
		dlcsApi.attachAllFiles(user.getToken());
		String tid = dlcsApi.searchTicketId(user.getUserId());
		tid = dlcsApi.startReview(tid);
		assertThat(dlcsApi.approve(tid)).isEqualTo("approved");
	}

	@Test
	public void addPaymentToProfileTest() {
		User user = usersApi.register();
		String token = authApi.login(user.getEmailAddress(), user.getPassword());
		user.setToken(token);

		String sig = paymentApi.signature(user.getUserId(), user.getToken());
		assertThat(paymentApi.register(sig, user.getFirstName() + " " + user.getLastName())).isEqualTo("AUTHORIZED");

		boolean hasWallet = false;
		for (int i = 0; i < 10; i++) {
			try {
				Thread.sleep(1000);
				user.mergeIfEmpty(usersApi.getUser(user.getToken()));
				if (user.getAnyPayment() != null) {
					hasWallet = true;
					break;
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		assertTrue(hasWallet);

		usersApi.changeProfilePayment(user.getToken(), user.getPreselectedProfile().getId(), user.getAnyPayment().getId());
	}
}
