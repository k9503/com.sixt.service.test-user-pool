package com.sixt.service.test_user_pool.handler;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.UUID;

import com.sixt.service.test_user_pool.api.GetUserRequest;
import com.sixt.service.test_user_pool.api.GetUserResponse;
import com.sixt.service.test_user_pool.api.UserType;
import com.sixt.service.test_user_pool.application.TestUserPoolService;
import com.sixt.service.test_user_pool.domain.TestUser;
import com.sixt.service.test_user_pool.domain.UndefinedTestCaseIdException;
import com.sixt.service.test_user_pool.domain.UndefinedUserTypeException;

import org.assertj.core.api.Assertions;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import spring.boot.starter.sixtgoorange.copiedfromjamicro.OrangeContext;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.RpcCallException;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@Import({GetUserHandler.class})
public class GetUserHandlerTest {

	@Autowired
	private GetUserHandler handler;

	@MockBean
	private TestUserPoolService service;

	@Test
	public void handleRequest_success() throws Exception {
		String tcid = new Exception().getStackTrace()[0].getClassName();

		GetUserRequest request = GetUserRequest.newBuilder()
			.setUserType(UserType.VANILLA)
			.setTcId(tcid)
			.build();

		when(service.getUserFromPool(eq(UserType.VANILLA), eq(tcid)))
			.thenReturn(new TestUser(tcid, UUID.randomUUID().toString()));

		GetUserResponse response = handler.handleRequest(request, new OrangeContext());
		assertThat(response.getName().contains("GetUserHandlerTest")).isTrue();
		assertThat(response.getPwd()).isNotEmpty();

		verify(service, times(1)).getUserFromPool(UserType.VANILLA, tcid);
	}

	@Test
	public void handleRequest_fail_UndefinedTestCaseId() {
		GetUserRequest request = GetUserRequest.newBuilder().build();

		RpcCallException rpcCallException = Assertions.catchThrowableOfType(
				() -> handler.handleRequest(request, new OrangeContext()), RpcCallException.class);
		assertThat(rpcCallException.getCategory()).isEqualTo(RpcCallException.Category.BadRequest);
		assertThat(rpcCallException.getErrorCode()).isEqualTo(UndefinedTestCaseIdException.ERR_CODE);
	}

	@Test
	@Ignore
	public void handleRequest_fail_UndefinedUserType() {
		GetUserRequest request = GetUserRequest.newBuilder()
			.setUserType(null) //
			.build();

		RpcCallException rpcCallException = Assertions.catchThrowableOfType(
				() -> handler.handleRequest(request, new OrangeContext()), RpcCallException.class);
		assertThat(rpcCallException.getCategory()).isEqualTo(RpcCallException.Category.BadRequest);
		assertThat(rpcCallException.getErrorCode()).isEqualTo(UndefinedUserTypeException.ERR_CODE);
	}

}
