pipeline {
  agent any
  options {
    disableConcurrentBuilds()
    timeout(time: 5, unit: 'MINUTES')
  }
  parameters {
    booleanParam(name: 'DEPLOY_DEV', defaultValue: env.BRANCH_NAME == 'master', description: 'Deploy to dev without manual approval')
  }
  environment {
    SERVICE_NAME = "${env.GIT_URL.replaceAll('^.*/', '').replaceAll('.git$', '')}"
    DOCKER_TAG = sh(returnStdout: true, script: './gradlew dockerTag -q').trim()
    DEPLOY_DEV = "${params.DEPLOY_DEV}"
  }

  stages {
    stage('Test') {
      steps {
        sshagent(credentials: ['bitbucket_ssh']) {
          sh './gradlew check'
        }
      }
      post {
        always {
          junit allowEmptyResults: true, testResults: 'build/test-results/**/*.xml', healthScaleFactor: 1.0
        }
      }
    }
    stage('Build') {
      steps {
        sshagent(credentials: ['bitbucket_ssh']) {
          sh './gradlew assemble'
        }
        script {
          currentBuild.displayName = '#' + currentBuild.number + ': ' + DOCKER_TAG
        }
      }
    }
    stage('Dockerization') {
      steps {
        script {
          sh './gradlew dockerBuild'
          withDockerRegistry([credentialsId: 'registry', url: "https://${env.REGISTRY_URL}"]) {
            sh './gradlew dockerPush'
          }
          sh './gradlew dockerClean'
        }
      }
    }

    stage('Confirm') {
      when { expression { DEPLOY_DEV == "false" } }
      steps {
        script {
          try {
            timeout(time: 60, unit: 'SECONDS') {
              input "Would you like to deploy ${SERVICE_NAME} version ${DOCKER_TAG} to dev?"
              DEPLOY_DEV = "true"
            }
          } catch (err) { }
        }
      }
    }

    stage('Deploy') {
      when { expression { DEPLOY_DEV != "false" } }
      steps {
        echo "deploy ${SERVICE_NAME} ${DOCKER_TAG} to ${params.DEPLOY_DEV}"
        deploy serviceName: SERVICE_NAME, version: DOCKER_TAG, gitBranch: GIT_BRANCH
      }
    }

  }

  post {
    always {
      cleanWs()
      dir("${env.WORKSPACE}@tmp") { deleteDir() }
    }
  }
}
