# Test user pool  #
`#JAVA` `#SPRINGBOOT` `#ONE` `#QA`

Maintained by [One QA](mailto:xiaoning.cheng@sixt.com)

Latest release [v1.8.0](https://bitbucket.org/sixtgoorange/com.sixt.service.test-user-pool/commits/tag/v.1.8.0)

## Service description ##

Service test-user-pool maintains a pool of **ready-to-use**, **one-time-disposable** test user. It's a _short cut_ of test user preparation. It provides a convenient way to get a test *login*(username:password pair).

To fit your test requirement, you can specify the `type` of the user you get. It supports:

| Type name	| Description	| Environment | Max pool size	|
|:---			|:---				|:---			|:---:		|
|`CLASSIC`| An new registered user in classic system which is not yet imported into GoOrange. | _dev_ | * |
|`VANILLA`| A new registered user. Email confirmed. | _dev_, _stage_ | 500 |
|`SAC`  | A new registered user has all the documents approved for using Share-A-Car. | _dev_, _stage_ | 500 |
|`SAC_DE`  | A new registered user has all the documents approved for using Share-A-Car with German language. | _dev_, _stage_ | 500 |
|`SAC_EN`  | A new registered user has all the documents approved for using Share-A-Car with English language. | _dev_, _stage_ | 500 |
|`SAC_IT`  | An Italy specific user type for product Share-A-Car. | _dev_, _stage_ | 500 |
|`SAC_NL`  | A Dutch specific user for product Share-A-Car. | _dev_, _stage_ | 500 |
|`SAC_AT`  | An Austria specific user for product Share-A-Car. | _dev_, _stage_ | 500 |
|`FASTLANE`  | A new registered user who is approved for using product Fastlane. | _dev_, _stage_ | 500 |
|`TIER`  | Approved to use product TIRE | Not ready | - |


## How to get a test user ##
#### With Micro-dashboard: ####
- From the [Sixt Playbook](https://playbook.goorange.sixt.com), choose the micro query tool and the service _com.sixt.service.test-user-pool_.
- Query method _TestUserPool.GetUser_ with payload like:
```json
{
	"user_type": "VANILLA",
	"tc_id": "my-test-case"
}
```
- In a success response, you get the email and password of the test user, like:
```json
{
	"name": "681d15a0-5b2c-46d2-b6fe-f30e84bb46fa@test-mailhog.com",
	"pwd": "dcgepKWKDK1!",
	"pin": "0000",
	"cc": "5520117488551476"
}
```

#### Within the code: ####
- For the convenience, we have created `TestUserPoolSteps` and related dto class in project [testautomation api](https://bitbucket.org/sixtgoorange/com.sixt.testautomation.api.one/src/master/).
- In the test method, get a _Login_ of the test user by calling:
```java
/* A demo test case to get a Share-A-Car ready user. */
@Steps TestUserPoolSteps testUserPoolSteps;
@Test
public void getTestUser_for_ShareACar() {
	Login login = testUserPoolSteps.getLogin(TestUserType.SAC, this.getClass().getName());
}
```
- Use the login to get a valid app token:
```java
@Steps AuthSteps authSteps;
Token token = authSteps.login(login.getName(), login.getPwd());
/* Your app test goes here... */
```

# Interface description
[Protobuffer definition](https://bitbucket.org/sixtgoorange/com.sixt.service.test-user-pool/src/master/src/main/proto/test-user-pool.proto)
To have the user type checked and even auto-completed in your IDE, reference the .proto file into your (e.g. Java) project under `src/main/proto`, and run:
```bash
./gradlew proto
```

# Service endpoints
### [rpc](https://bitbucket.org/sixtgoorange/com.sixt.service.test-user-pool/src/a74ee25db3a7b36253b4330ca6b6d3051242d582/src/main/proto/userpool.proto#lines-48) GetUser
Since version 1.0.0
GetUser gets a test user's login data in name:password pair. For example:

_Request_:
```javascript
{
	"user_type": "VANILLA",
	"tc_id": "my-test-case"
}
```
_Response_:

```javascript
{
	"name": "b4d7eb96-561c-4e85-a7f2-092a17498326@test-mailhog.com",
	"pwd": "dcgepKWKDK1!",
	"pin": "0000",
	"cc": "5520117488551476"
}
```

##### Request parameters
| Field name	| Description	| Data type	| Required	|
|:---				|:---				|:---			|:---:		|
|`user_type`| Type of the product the test user suite for. | enumeration | true|
|`tc_id`    | A signature of the test case or consumer. | string | true|

### [rpc](https://bitbucket.org/sixtgoorange/com.sixt.service.test-user-pool/src/a74ee25db3a7b36253b4330ca6b6d3051242d582/src/main/proto/userpool.proto#lines-51) LookupUser
_soon.._


# User types constants
- Email service provider: "@test-mailhog.com"
- Date of birth: "1977-01-01"
- Address DE: "Mueller Schmidt / Hauptstr. 10 / 80000 Munich / DE"
- Address IT: "Rossi Russo / Via Melzo 28 / 20129 Milano / IT"
- Address AT: "Franz Schubert / Lothringerstraße 20 / 1030 Wien / Österreich"
- PIN: "0000"
- Master card: "Pan: 5520117488551476 / Security code: 737 / Expiration: 2030-03"


# Service monitoring
- [Service Dashboard](https://metrics-dev.goorange.sixt.com/d/5yJY1LvWz/services-new-telemetry?orgId=1&refresh=10s&var-service_name=com.sixt.service.test-user-pool)
- Elastic search of warning on low level of available test user amount.
